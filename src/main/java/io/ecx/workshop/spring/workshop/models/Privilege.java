package io.ecx.workshop.spring.workshop.models;

import java.util.Collection;


import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Privilege extends AbstractPersistable<Integer>
{
    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Role> roles;

    public Privilege()
    {
        // Empty on purpose
    }

    public Privilege(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Collection<Role> getRoles()
    {
        return roles;
    }

    public void setRoles(Collection<Role> roles)
    {
        this.roles = roles;
    }
}
