package io.ecx.workshop.spring.workshop.service;

import java.util.List;
import java.util.Optional;


import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.models.User;

public interface BlogPostService
{
    Optional<BlogPost> findById(Integer id);

    BlogPost findByTitle(String title);

    List<BlogPost> findByUser(User user);

    BlogPost createBlogPost(String title, String content, User user, Category category);

    List<BlogPost> findAll();

    List<BlogPost> findAllActive();

    List<BlogPost> findAllInactive();

    List<BlogPost> findByCategory(Category category);

    Optional<BlogPost> toggleActive(Integer id);
}
