package io.ecx.workshop.spring.workshop.converters;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import io.ecx.workshop.spring.workshop.data.UserDetails;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.service.CommentService;

@Component
public class UserDetailsConverter implements Converter<User, UserDetails>
{
    private final CommentService commentService;

    @Autowired
    public UserDetailsConverter(CommentService commentService)
    {
        this.commentService = commentService;
    }

    @Override
    public UserDetails convert(User source)
    {
        UserDetails target = new UserDetails();

        if (!ObjectUtils.isEmpty(source))
        {
            target.setUser(source);
            try
            {
                target.setComments(filterActiveComments(commentService.findByUser(source)));
            }
            catch (NullPointerException e)
            {
                // silent catch
            }
        }

        return target;
    }

    private static List<Comment> filterActiveComments(final List<Comment> comments)
    {
        //List<Comment> list = new ArrayList<>();
        //for (Comment comment : comments)
        //{
        //    if (comment.isActive())
        //    {
        //        list.add(comment);
        //    }
        //}
        //return list;

        return comments.stream().filter(Comment::isActive).collect(Collectors.toList()); // ili filter(comment -> comment.isActive())
    }
}
