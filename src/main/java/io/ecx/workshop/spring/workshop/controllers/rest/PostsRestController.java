package io.ecx.workshop.spring.workshop.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.service.BlogPostService;

import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.Post.getActivePosts;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.Post.getInactivePosts;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.Post.postRoot;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.Post.togglePost;

/**
 * REST controller that handles operations over posts.
 */
@RestController
@RequestMapping(value = postRoot)
public class PostsRestController
{
    private final BlogPostService blogPostService;

    @Autowired
    public PostsRestController(BlogPostService blogPostService)
    {
        this.blogPostService = blogPostService;
    }

    /**
     * Retrieve all active posts.
     * @return JSON response with all active posts.
     */
    @GetMapping(value = getActivePosts)
    public ResponseEntity<List<BlogPost>> getActivePosts()
    {
        // TODO: return all active blog posts and set proper http status
        return new ResponseEntity<>(null);
    }

    /**
     * Retrieve all inactive posts.
     * @return JSON response with all inactive posts.
     */
    @GetMapping(value = getInactivePosts)
    public ResponseEntity<List<BlogPost>> getInactivePosts()
    {
        // TODO: return all inactive blog posts and set proper http status
        return new ResponseEntity<>(null);
    }

    /**
     * Toggles visibility of passed post.
     * @param id Id of the post
     * @return Response status
     */
    @PatchMapping(value = togglePost)
    public ResponseEntity togglePost(@PathVariable(value = "id") Integer id)
    {
        // TODO: change the active flag of the blog and return http status
        return new ResponseEntity(null);
    }
}
