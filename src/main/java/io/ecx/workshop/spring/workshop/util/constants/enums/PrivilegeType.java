package io.ecx.workshop.spring.workshop.util.constants.enums;

public enum PrivilegeType
{
    WRITE_PRIVILEGE,
    READ_PRIVILEGE
}
