package io.ecx.workshop.spring.workshop.util.constants;

import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.categoryAll;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.userAll;

public class ApiPathConstants
{
    public static final String api = "/api";

    public static class SecurityPaths
    {
        public static final String apiCategoryAll = api + categoryAll;

        public static final String apiUserAll = api + userAll;
    }

    public static class User
    {
        public static final String userRoot = api + "/user";

        public static final String getUser = "/{id}";

        public static final String getUserDetails = getUser + "/details";

        public static final String getSelfDetails = "/self";

        public static final String getActiveUsers = "/users/active";

        public static final String getInactiveUsers = "/users/inactive";

        public static final String toggleUser = "/{id}/toggle";
    }

    public static class Post
    {
        public static final String postRoot = api + "/post";

        public static final String getActivePosts = "/posts/active";

        public static final String getInactivePosts = "/posts/inactive";

        public static final String togglePost = "/{id}/toggle";
    }

    public static class Comments
    {
        public static final String commentsRoot = api + "/comments";

        public static final String saveBlogComment = "/{id}/blog";

        public static final String saveSubComment = "/{id}/comment";
    }

    public static class Category
    {
        public static final String categoryRoot = api + "/category";

        public static final String getActiveCategories = "/categories/active";
    }
}
