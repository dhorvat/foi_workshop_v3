package io.ecx.workshop.spring.workshop.controllers.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.forms.NewUserForm;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.service.MailService;
import io.ecx.workshop.spring.workshop.service.UserService;
import io.ecx.workshop.spring.workshop.util.BaseUtils;
import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.util.constants.ViewConstants;

import javax.validation.Valid;

/**
 * Controller that handles views for users administration.
 */
@Controller
@RequestMapping(value = PathConstants.AdminPaths.adminRoot)
public class UserController
{
    private final UserService userService;

    private final MailService mailService;

    private final JavaMailSender mailSender;

    private final ModelAndView mav = new ModelAndView();

    @Autowired
    public UserController(UserService userService,
                          MailService mailService,
                          JavaMailSender mailSender)
    {
        this.userService = userService;
        this.mailService = mailService;
        this.mailSender = mailSender;
    }

    /**
     * Shows users administration page.
     * @return Model and view object that contains user page template reference.
     */
    @GetMapping(value = PathConstants.AdminPaths.users)
    public ModelAndView users()
    {
        NewUserForm newUserForm = new NewUserForm();
        mav.setViewName(ViewConstants.AdminPages.user);
        mav.getModelMap().addAttribute("newUserForm", newUserForm);

        return mav;
    }

    /**
     * Create new user from user form.
     * @param newUserForm User data
     * @param bindingResult Form errors
     * @param request Http request
     * @return View of users.
     */
    @PostMapping(value = PathConstants.AdminPaths.createUser)
    public ModelAndView createUser(@Valid NewUserForm newUserForm, BindingResult bindingResult)
    {
        mav.setViewName(PathConstants.Redirect.users);
        if (bindingResult.hasErrors())
        {
            mav.getModelMap().addAttribute("newUserForm", newUserForm);
        }
        else
        {

            String tempPass = BaseUtils.generateRandomPassword();
            User user = userService.createUser(
                    newUserForm.getUsername(),
                    newUserForm.getFirstName(),
                    newUserForm.getLastName(),
                    newUserForm.getEmail(),
                    tempPass,
                    true);

            SimpleMailMessage mailMessage = mailService.constructTempPassEmail(user, tempPass);
            mailSender.send(mailMessage);
        }
        return mav;
    }
}
