package io.ecx.workshop.spring.workshop.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.ecx.workshop.spring.workshop.data.UserDetails;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.security.service.SessionService;
import io.ecx.workshop.spring.workshop.service.UserService;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.User.getActiveUsers;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.User.getInactiveUsers;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.User.getSelfDetails;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.User.getUser;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.User.getUserDetails;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.User.toggleUser;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.User.userRoot;

/**
 * REST controller that handles operations over users.
 */
@RestController
@RequestMapping(value = userRoot)
public class UserRestController
{
    private final UserService userService;

    private final SessionService sessionService;

    private final Converter<User, UserDetails> userDetailsConverter;

    @Autowired
    public UserRestController(UserService userService, SessionService sessionService, Converter<User, UserDetails> userDetailsConverter)
    {
        this.userService = userService;
        this.sessionService = sessionService;
        this.userDetailsConverter = userDetailsConverter;
    }

    @GetMapping(value = getUser)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public ResponseEntity<User> getUser(@PathVariable("id") Integer id)
    {
        /** TODO
         * - find the user by ID
         * - if user is not found return proper http status
         * - if user is found return the user and set proper http status
         */

        return new ResponseEntity<>(null);
    }

    @PutMapping
    public ResponseEntity<User> updateUser(@RequestBody User user)
    {
        // TODO: update the user with the user data and return proper http status
        return new ResponseEntity<>(null);
    }

    @GetMapping(value = getUserDetails)
    public ResponseEntity<UserDetails> getUserDetails(@PathVariable("id") Integer id)
    {
        /** TODO
         * - find the user by ID
         * - if user not found return empty UserDetails instance and proper http status
         * - if user is found return UserDetails instance and proper http status
         */

        return new ResponseEntity<>(null);
    }

    /**
     * Get details of currently logged in user.
     * @return JSON response with data of currently logged in user.
     */
    @GetMapping(value = getSelfDetails)
    public ResponseEntity<User> getSelfUserData()
    {
        // TODO: return currently signed in User data.
        return new ResponseEntity<>(null);
    }

    /**
     * Retrieve all active users.
     * @return JSON response with all active users.
     */
    @GetMapping(value = getActiveUsers)
    public ResponseEntity<List<User>> getAllActiveUsers()
    {
        // TODO: return all active users and proper http status
        return new ResponseEntity<>(null);
    }

    /**
     * Retrieve all inactive users.
     * @return JSON response with all inactive users.
     */
    @GetMapping(value = getInactiveUsers)
    public ResponseEntity<List<User>> getAllInactiveUsers()
    {
        // TODO: return all inactive users and proper http status
        return new ResponseEntity<>(null);
    }

    /**
     * Toggle user active state.
     * @param id The user's id.
     * @return Response status.
     */
    @PatchMapping(value = toggleUser)
    public ResponseEntity toggleUser(@PathVariable(value = "id") Integer id)
    {
        // TODO: change active status of the user and return proper http status
        return new ResponseEntity(null);
    }
}
