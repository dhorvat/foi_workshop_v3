package io.ecx.workshop.spring.workshop.forms;


import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class NewBlogPostForm
{
    @NotNull
    @NotBlank
    private String title;

    @NotNull
    @NotBlank
    private String content;

    @NotNull
    private Integer categoryId;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(final String content)
    {
        this.content = content;
    }

    public Integer getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(final Integer categoryId)
    {
        this.categoryId = categoryId;
    }
}
