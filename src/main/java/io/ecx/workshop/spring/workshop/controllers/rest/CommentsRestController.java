package io.ecx.workshop.spring.workshop.controllers.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.service.BlogPostService;
import io.ecx.workshop.spring.workshop.service.CommentService;

import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.Comments.commentsRoot;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.Comments.saveBlogComment;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.Comments.saveSubComment;

/**
 * REST controller that handles operations over comments.
 */
@RestController
@RequestMapping(value = commentsRoot)
public class CommentsRestController
{
    private final CommentService commentService;

    private final BlogPostService blogPostService;

    @Autowired
    public CommentsRestController(CommentService commentService,
                                  BlogPostService blogPostService)
    {
        this.commentService = commentService;
        this.blogPostService = blogPostService;
    }

    /**
     * Save root blog post comment.
     * @param blogPostId Blog post's id to which comment is associated with.
     * @param content Content of comment.
     * @return Status of saving comment.
     */
    @PostMapping(value = saveBlogComment)
    public ResponseEntity saveBlogComment(@PathVariable("id") Integer blogPostId, @RequestParam String content)
    {
        /** TODO
         * - find blog by id
         * - assign new comment to the found blog
         * - set proper http status based on the status of the comment creation logic
         */
        return new ResponseEntity(null);
    }

    /**
     * Save sub comment to blog post.
     * @param commentId Comment's id to which comment is associated with.
     * @param content Content of the comment.
     * @return Status of saving comment.
     */
    @PostMapping(value = saveSubComment)
    public ResponseEntity saveSubComment(@PathVariable("id") Integer commentId, @RequestParam String content)
    {
        /** TODO
         * - find the comment by ID
         * - assign the new comment to the parent comment
         * - set proper http status based on the status of the comment creation logic
         */
        return new ResponseEntity(null);
    }
}
