package io.ecx.workshop.spring.workshop.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import io.ecx.workshop.spring.workshop.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.models.Privilege;
import io.ecx.workshop.spring.workshop.models.Role;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.service.ModelService;
import io.ecx.workshop.spring.workshop.service.UserService;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import static io.ecx.workshop.spring.workshop.util.constants.enums.PrivilegeType.READ_PRIVILEGE;
import static io.ecx.workshop.spring.workshop.util.constants.enums.PrivilegeType.WRITE_PRIVILEGE;
import static io.ecx.workshop.spring.workshop.util.constants.enums.RoleType.ROLE_ADMIN;
import static io.ecx.workshop.spring.workshop.util.constants.enums.RoleType.ROLE_USER;

@Service
public class DBLoader
{
    private final UserRepository userRepository;

    private final PrivilegeRepository privilegeRepository;

    private final RoleRepository roleRepository;

    private final BlogPostRepository blogPostRepository;

    private final CommentRepository commentRepository;

    private final CategoryRepostory categoryRepostory;

    private final ModelService modelService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private boolean loaded;

    @Autowired
    public DBLoader(UserRepository userRepository,
                    PrivilegeRepository privilegeRepository,
                    RoleRepository roleRepository,
                    BlogPostRepository blogPostRepository,
                    CommentRepository commentRepository,
                    CategoryRepostory categoryRepostory,
                    ModelService modelService)
    {
        this.modelService = modelService;
        bCryptPasswordEncoder = new BCryptPasswordEncoder();
        loaded = false;
        this.userRepository = userRepository;
        this.privilegeRepository = privilegeRepository;
        this.roleRepository = roleRepository;
        this.blogPostRepository = blogPostRepository;
        this.commentRepository = commentRepository;
        this.categoryRepostory = categoryRepostory;
    }

    @Transactional
    @PostConstruct
    public void initData()
    {
        if (loaded)
            return;
        loaded = true;

        Privilege readPrivilege = createPrivilege(READ_PRIVILEGE.name());
        Privilege writePrivilege = createPrivilege(WRITE_PRIVILEGE.name());
        Collection<Privilege> adminPrivileges = Arrays.asList(readPrivilege, writePrivilege);
        Collection<Privilege> userPrivileges = Collections.singletonList(readPrivilege);
        Role adminRole = createRole(ROLE_ADMIN.name(), adminPrivileges);
        Role userRole = createRole(ROLE_USER.name(), userPrivileges);

        User admin = createUser("admin", bCryptPasswordEncoder.encode("admin"), "admin@admin.com", "Admin", "Adminiac", Collections.singletonList(adminRole), true);
        User user = createUser("user", bCryptPasswordEncoder.encode("pass"), "user@user.eu", "User", "Userin", Collections.singletonList(userRole), false);

        Category category = createCategory("Category1", "Category1 Description text", true);
        Category category1 = createCategory("Category2", "Category2 Description text", true);

        BlogPost blogPost1 = createBlogPost("BlogPostAdmin1", "<p>Bla bla paragraf bla</p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed aliquam arcu, sed placerat odio. Donec at tincidunt turpis. Vestibulum dapibus porta mauris vitae accumsan. Phasellus metus nulla, eleifend consectetur elit eu, bibendum scelerisque sapien. Morbi euismod eu augue a tincidunt. Donec non lobortis lectus. Pellentesque gravida, arcu vel faucibus maximus, mauris risus gravida purus, vel tempus quam lacus sed est. Aliquam erat volutpat. Integer a lorem eget nisi rutrum scelerisque. Donec ac augue sem. Vivamus vel vehicula ante. Nunc vitae auctor neque. Integer porta pulvinar tempus. Vivamus a tempus libero, ac vestibulum orci.", admin, true, category);
        BlogPost blogPost2 = createBlogPost("BlogPostAdmin2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed aliquam arcu, sed placerat odio. Donec at tincidunt turpis. Vestibulum dapibus porta mauris vitae accumsan. Phasellus metus nulla, eleifend consectetur elit eu, bibendum scelerisque sapien. Morbi euismod eu augue a tincidunt. Donec non lobortis lectus. Pellentesque gravida, arcu vel faucibus maximus, mauris risus gravida purus, vel tempus quam lacus sed est. Aliquam erat volutpat. Integer a lorem eget nisi rutrum scelerisque. Donec ac augue sem. Vivamus vel vehicula ante. Nunc vitae auctor neque. Integer porta pulvinar tempus. Vivamus a tempus libero, ac vestibulum orci.", admin, true, category1);

        Comment comment1 = createComment(1, "Root comment text for blogPost1", blogPost1, null, user, true);
        Comment comment11 = createComment(1, "Child comment of comment1", null, comment1, admin, true);
    }

    @Transactional
    public User createUser(String username, String password, String email, String firstName, String lastName, Collection<Role> roles, boolean active)
    {
        User user = userRepository.findByUsername(username);
        if (ObjectUtils.isEmpty(user))
        {
            user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setEmail(email);
            user.setLastName(lastName);
            user.setFirstName(firstName);
            user.setRoles(roles);
            user.setActive(active);
            modelService.save(user);
        }
        return user;
    }

    @Transactional
    public Privilege createPrivilege(String name)
    {
        Privilege privilege = privilegeRepository.findByName(name);
        if (ObjectUtils.isEmpty(privilege))
        {
            privilege = new Privilege(name);
            privilegeRepository.save(privilege);
        }

        return privilege;
    }

    @Transactional
    public Role createRole(String name, Collection<Privilege> privileges)
    {
        Role role = roleRepository.findByName(name);
        if (ObjectUtils.isEmpty(role))
        {
            role = new Role(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }

        return role;
    }

    @Transactional
    public BlogPost createBlogPost(String title, String content, User user, boolean active, Category category)
    {
        BlogPost blogPost = blogPostRepository.findByTitle(title);

        if (ObjectUtils.isEmpty(blogPost))
        {
            blogPost = new BlogPost();
            blogPost.setTitle(title);
            blogPost.setContent(content);
            blogPost.setUser(user);
            blogPost.setActive(active);
            blogPost.setCategory(category);
            modelService.save(blogPost);
        }

        return blogPost;
    }

    @Transactional
    public Comment createComment(Integer id, String content, BlogPost blogPost, Comment parentComment, User user, boolean active)
    {
        Comment comment = commentRepository.findById(id).orElse(null);

        if (ObjectUtils.isEmpty(comment))
        {
            comment = new Comment();

            comment.setContent(content);
            comment.setBlogPost(blogPost);
            comment.setComment(parentComment);
            comment.setUser(user);
            comment.setActive(active);
            modelService.save(comment);
        }

        return comment;
    }

    @Transactional
    public Category createCategory(String name, String description, boolean active)
    {
        Category category = categoryRepostory.findByName(name);

        if (ObjectUtils.isEmpty(category))
        {
            category = new Category();

            category.setName(name);
            category.setDescription(description);
            category.setActive(active);
            modelService.save(category);
        }

        return category;
    }
}
