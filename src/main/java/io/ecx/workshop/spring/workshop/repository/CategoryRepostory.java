package io.ecx.workshop.spring.workshop.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;

import io.ecx.workshop.spring.workshop.models.Category;

public interface CategoryRepostory extends JpaRepository<Category, Integer>
{
	List<Category> findByActive(boolean active);
	Category findByName(String name);
}
