package io.ecx.workshop.spring.workshop.data;

import java.util.List;

import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.models.User;

public class UserDetails
{
    private User user;

    private List<Comment> comments;

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public List<Comment> getComments()
    {
        return comments;
    }

    public void setComments(List<Comment> comments)
    {
        this.comments = comments;
    }
}
