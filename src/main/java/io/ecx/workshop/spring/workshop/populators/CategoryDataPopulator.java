package io.ecx.workshop.spring.workshop.populators;

import io.ecx.workshop.spring.workshop.data.CategoryData;
import io.ecx.workshop.spring.workshop.models.Category;

public interface CategoryDataPopulator
{
	void populate(Category category, CategoryData target);
}
