package io.ecx.workshop.spring.workshop.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.data.BlogPostDetails;
import io.ecx.workshop.spring.workshop.forms.NewUserForm;
import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.service.BlogPostService;
import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.util.constants.ViewConstants;

/**
 * Controller that handles post mappings.
 */
@Controller
public class PostController
{
    private final BlogPostService blogPostService;

    private final Converter<BlogPost, BlogPostDetails> blogPostDetailsConverter;

    private final ModelAndView mav = new ModelAndView();

    private final NewUserForm newUserForm = new NewUserForm();

    @Autowired
    public PostController(BlogPostService blogPostService,
                          Converter<BlogPost, BlogPostDetails> blogPostDetailsConverter)
    {
        this.blogPostService = blogPostService;
        this.blogPostDetailsConverter = blogPostDetailsConverter;
    }

    /**
     * Generates view for single blog post.
     * @param id Id of blog post to be shown.
     * @return Model and view object that displays blog post.
     */
    @GetMapping(value = PathConstants.BasePaths.post)
    public ModelAndView getPostPage(@PathVariable("id") Integer id)
    {
        /** TODO
         * - find blog by ID
         * - set view name to the model (hint: )
         * - set blog as a post model attribute
         */
        return mav;
    }
}
