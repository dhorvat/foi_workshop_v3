package io.ecx.workshop.spring.workshop.service.impl;

import java.util.List;
import java.util.Optional;

import org.hibernate.cfg.NotYetImplementedException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.repository.BlogPostRepository;
import io.ecx.workshop.spring.workshop.service.BlogPostService;
import io.ecx.workshop.spring.workshop.service.ModelService;

import static org.junit.Assert.assertEquals;

@Service
public class BlogPostServiceImpl implements BlogPostService
{
    private final BlogPostRepository blogPostRepository;

    private final ModelService modelService;

    @Autowired
    public BlogPostServiceImpl(BlogPostRepository blogPostRepository, ModelService modelService)
    {
        this.blogPostRepository = blogPostRepository;
        this.modelService = modelService;
    }

    @Test
    public void whenOrElseWorks_thenCorrect()
    {
        String nullName = null;
        String name = "";

        if (nullName != null)
        {
            name = nullName;
        }
        else
        {
            name = "john";
        }

        assertEquals("john", name);
    }

    @Override
    public Optional<BlogPost> findById(Integer id)
    {
        // TODO: call repository to find blog post by ID
        throw new NotYetImplementedException("findById");
    }

    @Override
    public BlogPost findByTitle(String title)
    {
        // TODO: call repository to find blog post by TITLE
        throw new NotYetImplementedException("findByTitle");
    }

    @Override
    public List<BlogPost> findByUser(User user)
    {
        // TODO: call repository to find all blog post by USER
        throw new NotYetImplementedException("findByUser");
    }

    @Override
    public BlogPost createBlogPost(String title, String content, User user, Category category)
    {
        /** TODO
         * - create new instance of BlogPost class
         * - populate title, content, user and category
         * - persist new BlogPost (hint: use modelService)
         * - return saved BlogPost
         */
        throw new NotYetImplementedException("createBlogPost");
    }

    @Override
    public List<BlogPost> findAll()
    {
        // TODO: call repostiry to fetch all blog posts
        throw new NotYetImplementedException("findAll");
    }

    @Override
    public List<BlogPost> findAllActive()
    {
        // TODO: call repository to fetch all active blog posts
        throw new NotYetImplementedException("findAllActive");
    }

    @Override
    public List<BlogPost> findAllInactive()
    {
        // TODO: call repository to fetch all inactive blog posts
        throw new NotYetImplementedException("findAllInactive");
    }

    @Override
    public List<BlogPost> findByCategory(final Category category)
    {
        // TODO: call repository to fetch all blog posts for the passed category
        throw new NotYetImplementedException("findByCategory");
    }

    @Override
    public Optional<BlogPost> toggleActive(Integer id)
    {
        /** TODO
         * - find blog post by ID
         * - change it's active flag (true -> false | false -> true)
         * - persist to database
         * - return updated blog post
         */
        throw new NotYetImplementedException("toggleActive");
    }
}
