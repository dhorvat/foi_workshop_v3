package io.ecx.workshop.spring.workshop.populators;

import io.ecx.workshop.spring.workshop.data.UserDetails;
import io.ecx.workshop.spring.workshop.models.User;

public interface UserDetailsPopulator
{
    void populate(final User source, final UserDetails target) throws NullPointerException;
}
