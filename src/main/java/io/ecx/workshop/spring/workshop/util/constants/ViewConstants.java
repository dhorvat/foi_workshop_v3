package io.ecx.workshop.spring.workshop.util.constants;

public class ViewConstants
{
    public static class Pages
    {
        public static final String home = "home";

        public static final String login = "login";

        public static final String login_error = "login-error";

        public static final String logout = "logout";

        public static final String profile = "profile";

        public static final String category = "category";

        public static final String post = "post";
    }

    public static class AdminPages
    {
        public static final String user = "admin/user";

        public static final String posts = "admin/post";

        public static final String category = "admin/category";

        public static final String footer = "admin/footer";
    }
}
