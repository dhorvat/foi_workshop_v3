package io.ecx.workshop.spring.workshop.service;

import java.util.List;


import io.ecx.workshop.spring.workshop.exceptions.CategoryExistsException;
import io.ecx.workshop.spring.workshop.models.Category;

public interface CategoryService
{
	Category findById(Integer id);
	Category findByName(String name);
	List<Category> findAllActive();
	List<Category> findAllInactive();
	Category createCategory(String name, String description) throws CategoryExistsException;
}
