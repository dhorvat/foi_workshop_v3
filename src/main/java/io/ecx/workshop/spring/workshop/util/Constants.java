package io.ecx.workshop.spring.workshop.util;

import static io.ecx.workshop.spring.workshop.util.Constants.Paths.SecurityPaths.categoryAll;
import static io.ecx.workshop.spring.workshop.util.Constants.Paths.SecurityPaths.userAll;

public interface Constants
{
    interface Roles
    {
        String adminRole = "ROLE_ADMIN";
        String userRole = "ROLE_USER";
    }
    interface Privileges
    {
        String writePrivilege = "WRITE_PRIVILEGE";
        String readPrivilege = "READ_PRIVILEGE";
    }
    interface Views
    {
        interface Pages
        {
            String home = "home";
            String login = "login";
            String login_error = "login-error";
            String logout = "logout";
            String profile = "profile";
            String category = "category";
            String post = "post";
        }
        interface AdminPages
        {
            String user = "admin/user";
            String posts = "admin/post";
            String category = "admin/category";
            String footer = "admin/footer";
        }
    }
    interface Paths
    {
        interface BasePaths
        {
            String login = "/login";
            String logout = "/logout";
            String login_error = "/login-error";
            String home = "/home";
            String registration = "/registration";
            String category = "/category/{id}";
            String post = "/post/{id}";
        }
        interface SecurityPaths
        {
            String basePath = "/";
            String categoryAll = "/category/**";
            String postAll = "/post/**";
            String error = "/error";
            String webjarsAll = "/webjars/**";
            String cssAll = "/css/**";
            String jsAll = "/js/**";
            String anyPath = "/**";
            String userAll = "/user/**";
            String adminAll = "/admin/**";
        }
        interface Profile
        {
            String selfProfile = "/profile";
            String userProfile = "/profile/{id}";
        }
        interface ApiPaths
        {
            String api = "/api";
            interface SecurityPaths
            {
                String apiCategoryAll = api + categoryAll;
                String apiUserAll = api + userAll;
            }
            interface User
            {
                String userRoot = api + "/user";
                String getUser = "/{id}";
                String getUserDetails = getUser + "/details";
                String getSelfDetails = "/self";
                String getActiveUsers = "/users/active";
                String getInactiveUsers = "/users/inactive";
                String toggleUser = "/{id}/toggle";
            }
            interface Post
            {
                String postRoot = api + "/post";
                String getActivePosts = "/posts/active";
                String getInactivePosts = "/posts/inactive";
                String togglePost = "/{id}/toggle";
            }
            interface Comments
            {
                String commentsRoot = api + "/comments";
                String saveBlogComment = "/{id}/blog";
                String saveSubComment = "/{id}/comment";
            }
            interface Category
            {
                String categoryRoot = api + "/category";
                String getActiveCategories = "/categories/active";
            }
        }
        interface AdminPaths
        {
            String adminRoot = "/admin";
            String users = "/users";
            String posts = "/posts";
            String savePost = "/savePost";
            String createUser = "/createUser";
            String category = "/category";
            String createCategory = "/createCategory";
            String footer = "/footer";
        }
        interface Redirect
        {
            String login = "redirect:".concat(BasePaths.login);
            String home = "redirect:".concat(BasePaths.home);
            String loginError = "redirect:".concat(BasePaths.login_error);
            String users = "redirect:".concat(AdminPaths.adminRoot + AdminPaths.users);
            String posts = "redirect:".concat(AdminPaths.adminRoot + AdminPaths.posts);
            String category = "redirect:".concat(AdminPaths.adminRoot + AdminPaths.category);
        }
    }
}
