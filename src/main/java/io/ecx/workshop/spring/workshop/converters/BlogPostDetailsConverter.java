package io.ecx.workshop.spring.workshop.converters;

import java.util.stream.Collectors;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import io.ecx.workshop.spring.workshop.data.BlogPostDetails;
import io.ecx.workshop.spring.workshop.data.CommentsData;
import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.service.CommentService;

@Component
public class BlogPostDetailsConverter implements Converter<BlogPost, BlogPostDetails>
{
    private final CommentService commentService;

    public BlogPostDetailsConverter(CommentService commentService)
    {
        this.commentService = commentService;
    }

    @Override
    public BlogPostDetails convert(BlogPost source)
    {
        BlogPostDetails target = new BlogPostDetails();
        target.setBlogPost(source);

        //List<CommentsData> list = new ArrayList<>();
        //for (Comment comment : commentService.findByBlogPost(source))
        //{
        //    CommentsData commentsData = getCommentsData(comment);
        //    list.add(commentsData);
        //}
        //target.setCommentsData(list);

        target.setCommentsData(commentService.findByBlogPost(source).stream()
                .map(this::getCommentsData)  // ili .map(comment -> getCommentsData(comment))
                .collect(Collectors.toList()));

        return target;
    }

    private CommentsData getCommentsData(final Comment comment)
    {
        CommentsData commentsData = new CommentsData();
        commentsData.setComment(comment);
        commentsData.setSubComments(commentService.findChildComments(comment));
        return commentsData;
    }
}
