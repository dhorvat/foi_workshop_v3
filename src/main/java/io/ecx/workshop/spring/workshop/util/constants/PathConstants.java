package io.ecx.workshop.spring.workshop.util.constants;

import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.AdminPaths.adminRoot;

public class PathConstants
{
    public static class BasePaths
    {
        public static final String login = "/login";

        public static final String logout = "/logout";

        public static final String login_error = "/login-error";

        public static final String home = "/home";

        public static final String registration = "/registration";

        public static final String category = "/category/{id}";

        public static final String post = "/post/{id}";
    }

    public static class SecurityPaths
    {
        public static final String basePath = "/";

        public static final String categoryAll = "/category/**";

        public static final String postAll = "/post/**";

        public static final String error = "/error";

        public static final String webjarsAll = "/webjars/**";

        public static final String cssAll = "/css/**";

        public static final String jsAll = "/js/**";

        public static final String anyPath = "/**";

        public static final String userAll = "/user/**";

        public static final String adminAll = "/admin/**";
    }

    public static class Profile
    {
        public static final String selfProfile = "/profile";

        public static final String userProfile = "/profile/{id}";
    }

    public static class AdminPaths
    {
        public static final String adminRoot = "/admin";

        public static final String users = "/users";

        public static final String posts = "/posts";

        public static final String savePost = "/savePost";

        public static final String createUser = "/createUser";

        public static final String category = "/category";

        public static final String createCategory = "/createCategory";

        public static final String footer = "/footer";
    }

    public static class Redirect
    {
        public static final String login = "redirect:".concat(BasePaths.login);

        public static final String home = "redirect:".concat(BasePaths.home);

        public static final String loginError = "redirect:".concat(BasePaths.login_error);

        public static final String users = "redirect:".concat(adminRoot + AdminPaths.users);

        public static final String posts = "redirect:".concat(adminRoot + AdminPaths.posts);

        public static final String category = "redirect:".concat(adminRoot + AdminPaths.category);
    }
}
