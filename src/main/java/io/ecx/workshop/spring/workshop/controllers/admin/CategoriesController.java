package io.ecx.workshop.spring.workshop.controllers.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.service.CategoryService;
import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.util.constants.ViewConstants;

/**
 * Controller that handles views for categories administration.
 */
@Controller
@RequestMapping(value = PathConstants.AdminPaths.adminRoot)
public class CategoriesController
{
    private final CategoryService categoryService;

    private final ModelAndView mav = new ModelAndView();

    @Autowired
    public CategoriesController(CategoryService categoryService)
    {
        this.categoryService = categoryService;
    }

    /**
     * Show category administration page.
     * @return Model and view object to show category administration page.
     */
    @GetMapping(value = PathConstants.AdminPaths.category)
    public ModelAndView showCategoryPage()
    {
        mav.setViewName(ViewConstants.AdminPages.category);
        mav.getModelMap().addAttribute("activeCategories", categoryService.findAllActive());
        mav.getModelMap().addAttribute("inactiveCategories", categoryService.findAllInactive());

        return mav;
    }

    /**
     * Create new category from parameters.
     * @param name Name of the new category.
     * @param description Description of the new category.
     * @return Model and view object to show category administration page.
     */
    @PostMapping(value = PathConstants.AdminPaths.createCategory)
    public ModelAndView createCategory(@RequestParam("name") String name, @RequestParam("description") String description)
    {
        mav.setViewName(PathConstants.Redirect.category);

        categoryService.createCategory(name, description);

        return mav;
    }
}
