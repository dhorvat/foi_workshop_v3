package io.ecx.workshop.spring.workshop.controllers;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.data.BlogPostDetails;
import io.ecx.workshop.spring.workshop.forms.NewUserForm;
import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.service.BlogPostService;
import io.ecx.workshop.spring.workshop.service.UserService;
import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.validators.RegisterUserFieldMatchValidator;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Controller that handles registration mappings.
 */
@Controller
public class RegistrationController
{
    private final RegisterUserFieldMatchValidator fieldMatchValidator;

    private final Converter<BlogPost, BlogPostDetails> blogPostDetailsConverter;

    private final BlogPostService blogPostService;

    private final UserService userService;

    private final ModelAndView mav = new ModelAndView();

    @Autowired
    public RegistrationController(RegisterUserFieldMatchValidator fieldMatchValidator,
                                  Converter<BlogPost, BlogPostDetails> blogPostDetailsConverter,
                                  BlogPostService blogPostService,
                                  UserService userService)
    {
        this.fieldMatchValidator = fieldMatchValidator;
        this.blogPostDetailsConverter = blogPostDetailsConverter;
        this.blogPostService = blogPostService;
        this.userService = userService;
    }

    /**
     * Register new user in application.
     * @param newUserForm Form with user data.
     * @param bindingResult Form errors.
     * @param request Http request
     * @return Model and view object to show registration details.
     */
    @PostMapping(value = PathConstants.BasePaths.registration)
    public ModelAndView register(@Valid NewUserForm newUserForm, BindingResult bindingResult, HttpServletRequest request)
    {
        ValidationUtils.invokeValidator(fieldMatchValidator, newUserForm, bindingResult);

        List<BlogPostDetails> blogPosts = blogPostService.findAllActive().stream().map(blogPostDetailsConverter::convert).collect(Collectors.toList());
        mav.getModelMap().addAttribute("blogPosts", blogPosts);
        mav.getModelMap().addAttribute("submited", true);
        mav.setViewName(PathConstants.BasePaths.home);

        if (!bindingResult.hasErrors())
        {
            userService.createUser(newUserForm.getUsername(), newUserForm.getFirstName(), newUserForm.getLastName(), newUserForm.getEmail(), newUserForm.getPassword(), false);
        }

        mav.getModelMap().addAttribute("newUserForm", newUserForm);

        return mav;
    }
}
