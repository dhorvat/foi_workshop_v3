package io.ecx.workshop.spring.workshop.util.constants.enums;

public enum RoleType
{
    ROLE_ADMIN,
    ROLE_USER
}
