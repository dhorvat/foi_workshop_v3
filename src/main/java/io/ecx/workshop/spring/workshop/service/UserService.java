package io.ecx.workshop.spring.workshop.service;

import java.util.List;
import java.util.Optional;

import io.ecx.workshop.spring.workshop.exceptions.UserNotFoundException;
import io.ecx.workshop.spring.workshop.models.User;

public interface UserService
{
    User findByUsername(String username);

    User findByEmail(String email);

    Optional<User> findById(Integer id);

    User createUser(String username, String firstName, String lastName, String email, String passwordPlain, boolean active);

    List<User> findByActive(boolean active);

    User updateUser(User user);

    Optional<User> toggleActive(Integer id);
}
