package io.ecx.workshop.spring.workshop.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.thymeleaf.util.StringUtils;

import io.ecx.workshop.spring.workshop.forms.NewUserForm;
import io.ecx.workshop.spring.workshop.service.UserService;

@Component("RegisterUserFieldMatchValidator")
public class RegisterUserFieldMatchValidator implements Validator
{
    private final UserService userService;

    @Autowired
    public RegisterUserFieldMatchValidator(UserService userService)
    {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass)
    {
        return NewUserForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors)
    {
        final NewUserForm newUserForm = (NewUserForm) o;

        if (!ObjectUtils.isEmpty(userService.findByUsername(newUserForm.getUsername())))
        {
            errors.rejectValue("username", "username.exists", "Username is taken!");
        }

        if (!StringUtils.equals(newUserForm.getPassword(), newUserForm.getrPassword()))
        {
            errors.rejectValue("password", "password.notmatch", "Passwords do not match");
        }
    }
}
