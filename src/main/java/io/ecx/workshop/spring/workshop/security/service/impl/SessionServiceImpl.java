package io.ecx.workshop.spring.workshop.security.service.impl;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import io.ecx.workshop.spring.workshop.security.service.SessionService;

@Service
public class SessionServiceImpl implements SessionService
{
    @Override
    public String getActiveUsername()
    {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
