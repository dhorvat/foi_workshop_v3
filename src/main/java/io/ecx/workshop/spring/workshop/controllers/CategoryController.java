package io.ecx.workshop.spring.workshop.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.data.CategoryData;
import io.ecx.workshop.spring.workshop.forms.NewUserForm;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.service.CategoryService;
import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.util.constants.ViewConstants;

/**
 * Controller that handles category mappings.
 */
@Controller
public class CategoryController
{
    private final CategoryService categoryService;

    private final Converter<Category, CategoryData> categoryConverter;

    private final ModelAndView mav = new ModelAndView();

    private final NewUserForm newUserForm = new NewUserForm();

    @Autowired
    public CategoryController(CategoryService categoryService,
                              Converter<Category, CategoryData> categoryConverter)
    {
        this.categoryService = categoryService;
        this.categoryConverter = categoryConverter;
    }

    /**
     * Show details of selected category.
     * @param id Id of category to show
     * @return Model and view object for category details.
     */
    @GetMapping(value = PathConstants.BasePaths.category)
    public ModelAndView getCategoryDetails(@PathVariable("id") Integer id)
    {
        /** TODO
         * - find the category by ID
         * - set view name (hint: ViewConstants)
         * - set the newUserForm attribute
         * - set categoryData model attribute
         */

        return mav;
    }
}
