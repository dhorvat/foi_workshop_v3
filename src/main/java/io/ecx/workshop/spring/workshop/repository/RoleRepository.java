package io.ecx.workshop.spring.workshop.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import io.ecx.workshop.spring.workshop.models.Privilege;
import io.ecx.workshop.spring.workshop.models.Role;

public interface RoleRepository extends JpaRepository<Role, Integer>
{
    Role findByName(String name);
    Collection<Role> findByPrivileges(Collection<Privilege> privileges);
}
