package io.ecx.workshop.spring.workshop.exceptions;

public class DeleteFailedException extends RuntimeException
{
    public DeleteFailedException(String message)
    {
        super(message);
    }
}
