package io.ecx.workshop.spring.workshop.exceptions;

public class CategoryExistsException extends RuntimeException
{
    public CategoryExistsException(String message)
    {
        super(message);
    }
}
