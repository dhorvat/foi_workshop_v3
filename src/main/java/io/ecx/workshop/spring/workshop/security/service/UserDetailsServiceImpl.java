package io.ecx.workshop.spring.workshop.security.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import io.ecx.workshop.spring.workshop.models.Privilege;
import io.ecx.workshop.spring.workshop.models.Role;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.service.UserService;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService
{
	private final UserService userService;

	@Autowired
	public UserDetailsServiceImpl(UserService userService)
	{
		this.userService = userService;
	}

	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException
	{

		User user = userService.findByUsername(s);
		if (user == null)
		{
			return new org.springframework.security.core.userdetails.User(" ", " ", new HashSet<>());
		}
		else
		{
			return new org.springframework.security.core.userdetails.User(
																		   user.getUsername(),
																		   user.getPassword(),
																		   user.isActive(),
																		   true,
																		   true,
																		   true,
																		   getAuthorities(user.getRoles())
			);
		}
	}

	private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles)
	{
		return getGrantedAuthorities(getAllPrivileges(roles));
	}

	private List<String> getAllPrivileges(Collection<Role> roles)
	{
		List<String> privileges = new ArrayList<>();

		//List<String> list = new ArrayList<>();
		//for (Role role : roles)
		//{
		//	for (Privilege privilege : role.getPrivileges())
		//	{
		//		String name = privilege.getName();
		//		list.add(name);
		//	}
		//}
		//privileges.addAll(list);

		privileges.addAll(roles.stream().flatMap(role -> role.getPrivileges().stream()).map(Privilege::getName).collect(Collectors.toList()));
		// ili .map(privilege -> privilege.getName())
		//READ_PRIVILEGE i WRITE_PRIVILEGE

		//List<String> list = new ArrayList<>();
		//for (Role role : roles)
		//{
		//	String name = role.getName();
		//	list.add(name);
		//}
		//privileges.addAll(list);

		privileges.addAll(roles.stream().map(Role::getName).collect(Collectors.toList()));
		// ili .map(role -> role.getName())

		return privileges;
	}

	private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges)
	{
		//List<GrantedAuthority> list = new ArrayList<>();
		//for (String privilege : privileges)
		//{
		//	SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(privilege);
		//	list.add(simpleGrantedAuthority);
		//}
		//return list;

		return privileges
				 .stream()
				 .map(SimpleGrantedAuthority::new)
				 .collect(Collectors.toList());
		//ili .map(role -> new SimpleGrantedAuthority(role))
	}
}
