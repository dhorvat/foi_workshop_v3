package io.ecx.workshop.spring.workshop.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import io.ecx.workshop.spring.workshop.exceptions.UserNotFoundException;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.repository.RoleRepository;
import io.ecx.workshop.spring.workshop.repository.UserRepository;
import io.ecx.workshop.spring.workshop.service.ModelService;
import io.ecx.workshop.spring.workshop.service.UserService;

import static io.ecx.workshop.spring.workshop.util.constants.enums.RoleType.ROLE_USER;

@Service
public class UserServiceImpl implements UserService
{
    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final ModelService modelService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder,
                           ModelService modelService)
    {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.modelService = modelService;
    }

    @Override
    public User findByUsername(String username)
    {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByEmail(String email)
    {
        // TODO: find the user by email

        throw new NotYetImplementedException("findByEmail");
    }

    @Override
    public Optional<User> findById(Integer id)
    {
        // TODO: return the user by id, or empty if not found

        throw new NotYetImplementedException("findById");
    }

    @Override
    public User createUser(String username, String firstName, String lastName, String email, String passwordPlain, boolean active)
    {
        /** TODO
         * - create new instance of the User class
         * - populate it's data (hint: set Rols to ROLE_USER enum, encode the password with bCryptPasswordEncoder)
         * - save new user and return it
         */

        throw new NotYetImplementedException("createUser");
    }

    @Override
    public List<User> findByActive(boolean active)
    {
        // TODO: return all active users

        throw new NotYetImplementedException("findByActive");
    }

    @Override
    public User updateUser(User user)
    {
        /** TODO
         * - find the user by id, if not found throw new UserNotFoundException
         * - populate the user the found user with new data
         * - save the user data and return it
         */

        throw new NotYetImplementedException("updateUser");
    }

    @Override
    public Optional<User> toggleActive(Integer id)
    {
        /** TODO
         * - find the user by id
         * - toggle it's active flag (true -> false | false -> true)
         * - save the user and return it
         */

        throw new NotYetImplementedException("toggleActive");
    }
}
