package io.ecx.workshop.spring.workshop.populators.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import io.ecx.workshop.spring.workshop.data.UserDetails;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.populators.UserDetailsPopulator;
import io.ecx.workshop.spring.workshop.service.CommentService;

/**
 * Popultaes UserDetails instance with user details.
 */
@Component
public class DefaultUserDetailsPopulator implements UserDetailsPopulator
{
    @Autowired
    private CommentService commentService;

    /**
     * Populates UserDetails instance with user
     * @param source Source User instance.
     * @param target Target UserDetails instance.
     * @throws NullPointerException
     */
    @Override
    public void populate(User source, UserDetails target) throws NullPointerException
    {
        if(ObjectUtils.isEmpty(source))
        {
            return;
        }
        target.setUser(source);
        try
        {
            target.setComments(filterActiveComments(commentService.findByUser(source)));
        } catch (NullPointerException e)
        {
            throw new NullPointerException("Comments not found!");
        }
    }

    private List<Comment> filterActiveComments(final List<Comment> comments)
    {
        //List<Comment> list = new ArrayList<>();
        //for (Comment comment : comments)
        //{
        //    if (comment.isActive())
        //    {
        //        list.add(comment);
        //    }
        //}
        //return list;

        return comments.stream().filter(Comment::isActive).collect(Collectors.toList()); // ili filter(comment -> comment.isActive())
    }
}
