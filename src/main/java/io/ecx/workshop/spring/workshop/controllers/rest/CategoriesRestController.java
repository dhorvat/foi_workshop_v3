package io.ecx.workshop.spring.workshop.controllers.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.service.CategoryService;

import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.Category.categoryRoot;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.Category.getActiveCategories;

/**
 * REST controller that handles operations over categories.
 */
@RestController
@RequestMapping(value = categoryRoot)
public class CategoriesRestController
{
    private final CategoryService categoryService;

    @Autowired
    public CategoriesRestController(CategoryService categoryService)
    {
        this.categoryService = categoryService;
    }

    /**
     * Get all active categories.
     * @return JSON object with all categories.
     */
    @GetMapping(value = getActiveCategories)
    public ResponseEntity<List<Category>> getActiveCategories()
    {
        // TODO: return all active categories and set http status OK
        return new ResponseEntity<>(null);
    }
}
