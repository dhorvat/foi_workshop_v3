package io.ecx.workshop.spring.workshop.service.impl;

import java.util.List;

import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import io.ecx.workshop.spring.workshop.exceptions.CategoryExistsException;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.repository.CategoryRepostory;
import io.ecx.workshop.spring.workshop.service.CategoryService;
import io.ecx.workshop.spring.workshop.service.ModelService;

@Component
public class CategoryServiceImpl implements CategoryService
{
    private final CategoryRepostory categoryRepostory;

    private final ModelService modelService;

    @Autowired
    public CategoryServiceImpl(CategoryRepostory categoryRepostory,
                               ModelService modelService)
    {
        this.categoryRepostory = categoryRepostory;
        this.modelService = modelService;
    }

    @Override
    public Category findById(final Integer id)
    {
        // TODO: return category by ID
        throw new NotYetImplementedException("findById");
    }

    @Override
    public Category findByName(final String name)
    {
        // TODO: return category by name
        throw new NotYetImplementedException("findByName");
    }

    @Override
    public List<Category> findAllActive()
    {
        // TODO: return all active categories
        throw new NotYetImplementedException("findAllActive");
    }

    @Override
    public List<Category> findAllInactive()
    {
        // TODO: return all active categories
        throw new NotYetImplementedException("findAllInactive");
    }

    @Override
    public Category createCategory(String name, String description) throws CategoryExistsException
    {
        /** TODO
         * - check if category with passed name already exists, if exists throw new CategoryExistsException
         * - if category doesn't exist, populate the fields and set active flag to false
         * - save and return new category
         */

        throw new NotYetImplementedException("createCategory");
    }
}
