package io.ecx.workshop.spring.workshop.service;

import org.springframework.mail.SimpleMailMessage;

import io.ecx.workshop.spring.workshop.models.User;

public interface MailService
{
	SimpleMailMessage constructEmail(String subject, String body, String emailTo);
	SimpleMailMessage constructTempPassEmail(User user, String tempPass);
}
