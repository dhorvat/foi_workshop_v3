package io.ecx.workshop.spring.workshop.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.ecx.workshop.spring.workshop.util.constants.PathConstants;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler
{
    private final ModelAndView mav = new ModelAndView();

    @ExceptionHandler(CategoryExistsException.class)
    public final ModelAndView handleCategoryExistsException(CategoryExistsException e)
    {
        mav.setViewName(PathConstants.Redirect.category);
        return mav;
    }

    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity handleUserNotFoundException(UserNotFoundException e)
    {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
