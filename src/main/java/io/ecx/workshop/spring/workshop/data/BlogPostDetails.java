package io.ecx.workshop.spring.workshop.data;

import java.util.List;

import io.ecx.workshop.spring.workshop.models.BlogPost;

/**
 * Data object that contains expanded information of post.
 */
public class BlogPostDetails
{
    private BlogPost blogPost;

    private List<CommentsData> commentsData;

    public BlogPost getBlogPost()
    {
        return blogPost;
    }

    public void setBlogPost(BlogPost blogPost)
    {
        this.blogPost = blogPost;
    }

    public List<CommentsData> getCommentsData()
    {
        return commentsData;
    }

    public void setCommentsData(List<CommentsData> commentsData)
    {
        this.commentsData = commentsData;
    }
}
