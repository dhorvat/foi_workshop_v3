package io.ecx.workshop.spring.workshop.data;



/**
 * Data object containing basic blog post data.
 */
public class BlogPostData
{
    private Integer id;

    private String title;

    public Integer getId()
    {
        return id;
    }

    public void setId(final Integer id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }
}
