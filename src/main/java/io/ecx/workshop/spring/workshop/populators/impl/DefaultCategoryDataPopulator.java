package io.ecx.workshop.spring.workshop.populators.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import io.ecx.workshop.spring.workshop.data.BlogPostData;
import io.ecx.workshop.spring.workshop.data.CategoryData;
import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.populators.CategoryDataPopulator;
import io.ecx.workshop.spring.workshop.service.BlogPostService;

/**
 * Populator that populates CategoryData with Category and Blog post data.
 */
@Component
public class DefaultCategoryDataPopulator implements CategoryDataPopulator
{
	@Autowired
	private BlogPostService blogPostService;

	/**
	 * Populates CategoryData with category information and associated blog posts.
	 * @param category Category object instance.
	 * @param target Target CategoryData instance
	 */
	@Override
	public void populate(final Category category, final CategoryData target)
	{
		target.setId(category.getId());
		target.setName(category.getName());
		target.setDescription(category.getDescription());

		List<BlogPost> blogPosts = blogPostService.findByCategory(category);
		List<BlogPostData> blogPostDatas = new ArrayList<>();

		if(!CollectionUtils.isEmpty(blogPosts))
		{
			for(BlogPost blogPost : filterActive(blogPosts))
			{
				BlogPostData blogPostData = new BlogPostData();
				blogPostData.setId(blogPost.getId());
				blogPostData.setTitle(blogPost.getTitle());
				blogPostDatas.add(blogPostData);
			}
			target.setBlogPostData(blogPostDatas);
		}
	}

	/**
	 * Filters out inactive BlogPosts.
	 * @param blogPost List of blog's for filtering.
	 * @return Filtered list of blog's.
	 */
	private List<BlogPost> filterActive(final List<BlogPost> blogPost)
	{
		//List<BlogPost> list = new ArrayList<>();
		//for (BlogPost post : blogPost)
		//{
		//	if (post.isActive())
		//	{
		//		list.add(post);
		//	}
		//}
		//return list;

		return blogPost.stream().filter(BlogPost::isActive).collect(Collectors.toList());  // ili filter(blogPost1 -> blogPost1.isActive())
	}
}
