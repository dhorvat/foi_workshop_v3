package io.ecx.workshop.spring.workshop.service;

import org.springframework.data.jpa.domain.AbstractPersistable;

import io.ecx.workshop.spring.workshop.exceptions.DeleteFailedException;

public interface ModelService
{
    <T> T save(AbstractPersistable model);

    boolean delete(AbstractPersistable model) throws DeleteFailedException;
}
