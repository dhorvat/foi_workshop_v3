package io.ecx.workshop.spring.workshop.controllers.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.util.constants.ViewConstants;

/**
 * Controller that handles footer administration.
 */
@Controller
@RequestMapping(value = PathConstants.AdminPaths.adminRoot)
public class FooterController
{
    private final ModelAndView mav = new ModelAndView();

    /**
     * Show footer administration page.
     * @return Model and view object to show footer administration page.
     */
    @GetMapping(value = PathConstants.AdminPaths.footer)
    public ModelAndView showFooterPage()
    {
        mav.setViewName(ViewConstants.AdminPages.footer);

        return mav;
    }
}
