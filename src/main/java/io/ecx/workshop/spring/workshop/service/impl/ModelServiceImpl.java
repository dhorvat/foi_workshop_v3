package io.ecx.workshop.spring.workshop.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import io.ecx.workshop.spring.workshop.exceptions.DeleteFailedException;
import io.ecx.workshop.spring.workshop.service.ModelService;

/**
 * @author d.horvat
 * @since 13-08-2019
 */
@Service
public class ModelServiceImpl implements ModelService
{
    private Repositories repositories;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public ModelServiceImpl(WebApplicationContext webApplicationContext)
    {
        this.repositories = new Repositories(webApplicationContext);
    }

    @Override
    public <T> T save(AbstractPersistable model)
    {
        JpaRepository repository = getRepository(model);
        AbstractPersistable result = null;

        if (repository != null)
        {
            result = (AbstractPersistable) repository.save(model);
            logger.info("Saved item [%s] with ID [%s]", result.getClass().getSimpleName(), result.getId());
        }

        return (T) result;
    }

    @Override
    public boolean delete(AbstractPersistable model) throws DeleteFailedException
    {
        boolean status = false;
        JpaRepository repository = getRepository(model);

        if (repository != null)
        {
            try
            {
                repository.delete(model);
                status = true;
            }
            catch (Exception e)
            {
                throw new DeleteFailedException(String.format("Deletion failed for item of type [%s] and id [%s]",  model.getClass(), model.getId()));
            }
        }

        return status;
    }

    private JpaRepository getRepository(Object entity)
    {
        return (JpaRepository) repositories.getRepositoryFor(entity.getClass()).orElse(null);
    }
}
