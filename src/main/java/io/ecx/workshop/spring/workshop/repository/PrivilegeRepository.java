package io.ecx.workshop.spring.workshop.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import io.ecx.workshop.spring.workshop.models.Privilege;
import io.ecx.workshop.spring.workshop.models.Role;

public interface PrivilegeRepository extends JpaRepository<Privilege, Integer>
{
    Privilege findByName(String name);
    Collection<Privilege> findByRoles(Collection<Role> roles);
}
