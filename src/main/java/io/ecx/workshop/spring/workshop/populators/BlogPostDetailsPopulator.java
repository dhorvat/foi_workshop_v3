package io.ecx.workshop.spring.workshop.populators;

import java.util.List;

import io.ecx.workshop.spring.workshop.data.BlogPostDetails;
import io.ecx.workshop.spring.workshop.models.BlogPost;

public interface BlogPostDetailsPopulator
{
    void populate(List<BlogPost> source, List<BlogPostDetails> target);
    void populate(BlogPost source, BlogPostDetails target);
}
