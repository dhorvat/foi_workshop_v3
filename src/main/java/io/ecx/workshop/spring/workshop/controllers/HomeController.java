package io.ecx.workshop.spring.workshop.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.data.BlogPostDetails;
import io.ecx.workshop.spring.workshop.forms.NewUserForm;
import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.service.BlogPostService;
import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.util.constants.ViewConstants;

/**
 * Controller that handles homepage mappings.
 */
@Controller
public class HomeController
{
    private final BlogPostService blogPostService;

    private final Converter<BlogPost, BlogPostDetails> blogPostDetailsConverter;

    private final ModelAndView mav = new ModelAndView();

    @Autowired
    public HomeController(BlogPostService blogPostService,
                          Converter<BlogPost, BlogPostDetails> blogPostDetailsConverter)
    {
        this.blogPostService = blogPostService;
        this.blogPostDetailsConverter = blogPostDetailsConverter;
    }

    /**
     * Populate homepage with active blog posts.
     * @return Model and view object for home page.
     */
    @RequestMapping(value = PathConstants.BasePaths.home)
    public ModelAndView home()
    {
        /** TODO
         * - set view name to the model (hint: ViewConstants)
         * - set newUserForm attribute
         * - get all active blog posts, convert them to Data object
         * - add converted Data objects to the model blogPosts attribute
        */

        return mav;
    }

    /**
     * Redirects root path to /home path.
     * @return Redirection string.
     */
    @GetMapping("/")
    public String homeBlank()
    {
        // TODO: redirect to homepage (hint: PathConstants)
        return "";
    }
}
