package io.ecx.workshop.spring.workshop.service.impl;

import java.util.List;
import java.util.Optional;

import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.repository.CommentRepository;
import io.ecx.workshop.spring.workshop.security.service.SessionService;
import io.ecx.workshop.spring.workshop.service.CommentService;
import io.ecx.workshop.spring.workshop.service.ModelService;
import io.ecx.workshop.spring.workshop.service.UserService;

@Service
public class CommentServiceImpl implements CommentService
{
    private final CommentRepository commentRepository;

    private final ModelService modelService;

    private final UserService userService;

    private final SessionService sessionService;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository,
                              ModelService modelService,
                              UserService userService,
                              SessionService sessionService)
    {
        this.commentRepository = commentRepository;
        this.modelService = modelService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    public Comment findById(Integer id)
    {
        // TODO: return comment by ID or null if not found

        throw new NotYetImplementedException("findById");
    }

    @Override
    public List<Comment> findByBlogPost(BlogPost blogPost)
    {
        // TODO: return all comments for the blog post

        throw new NotYetImplementedException("findByBlogPost");
    }

    @Override
    public List<Comment> findByUser(User user)
    {
        // TODO: return all comments for the user

        throw new NotYetImplementedException("findByUser");
    }

    @Override
    public List<Comment> findChildComments(Comment comment)
    {
        // TODO: return all child comments

        throw new NotYetImplementedException("findChildComments");
    }

    @Override
    public Optional<Comment> createComment(User user, String content, BlogPost blogPost)
    {
        /** TODO
         * - Create new instance of the Comment class
         * - populate it with the passed data
         * - set it's active flag to false
         * - save new Comment instance and return it
         */

        throw new NotYetImplementedException("createComment");
    }

    @Override
    public Optional<Comment> createComment(User user, String content, Comment parentComment)
    {
        /** TODO
         * - Create new instance of the Comment class
         * - populate it with the passed data (hint: set parentComment to a comment field of the Comment instance)
         * - set it's active flag to false
         * - save new Comment instance and return it
         */

        throw new NotYetImplementedException("createComment");
    }

    @Override
    public Optional<Comment> createCommentForCurrentUser(String content, BlogPost blogPost)
    {
        /** TODO
         * - find the current user
         * - create a comment based on the passed data (hint: pass the data to the createComment method)
         */

        throw new NotYetImplementedException("createCommentForCurrentUser");
    }

    @Override
    public Optional<Comment> createCommentForCurrentUser(String content, Comment parentComment)
    {
        /** TODO
         * - find the current user
         * - create a sub-comment based on the passed data (hint: pass the data to the createComment method)
         */

        throw new NotYetImplementedException("createCommentForCurrentUser");
    }
}
