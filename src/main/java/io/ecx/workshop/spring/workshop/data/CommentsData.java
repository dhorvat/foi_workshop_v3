package io.ecx.workshop.spring.workshop.data;

import java.util.List;

import io.ecx.workshop.spring.workshop.models.Comment;

/**
 * Data object that contains expanded data of comments.
 */
public class CommentsData
{
    private Comment comment;

    private List<Comment> subComments;

    public Comment getComment()
    {
        return comment;
    }

    public void setComment(Comment comment)
    {
        this.comment = comment;
    }

    public List<Comment> getSubComments()
    {
        return subComments;
    }

    public void setSubComments(List<Comment> subComments)
    {
        this.subComments = subComments;
    }
}
