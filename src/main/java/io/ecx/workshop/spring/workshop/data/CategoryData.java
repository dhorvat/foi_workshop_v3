package io.ecx.workshop.spring.workshop.data;

import java.util.List;


/**
 * Data object with category data and associated blog posts.
 */
public class CategoryData
{
    private Integer id;

    private String name;

    private String description;

    private List<BlogPostData> blogPostData;

    public List<BlogPostData> getBlogPostData()
    {
        return blogPostData;
    }

    public void setBlogPostData(final List<BlogPostData> blogPostData)
    {
        this.blogPostData = blogPostData;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(final Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }
}
