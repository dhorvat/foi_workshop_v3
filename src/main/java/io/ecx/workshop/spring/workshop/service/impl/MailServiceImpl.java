package io.ecx.workshop.spring.workshop.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.service.MailService;

@Component
public class MailServiceImpl implements MailService
{
	private final Environment env;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	public MailServiceImpl(Environment env)
	{
		this.env = env;
	}

	@Override
	public SimpleMailMessage constructEmail(final String subject, final String body, final String emailTo)
	{
		SimpleMailMessage email = new SimpleMailMessage();

		email.setSubject(subject);
		email.setText(body);
		email.setTo(emailTo);
		email.setFrom(env.getProperty("spring.mail.username"));

		logger.info("Constructed email for users [%s]", email.getTo());
		return email;
	}

	@Override
	public SimpleMailMessage constructTempPassEmail(final User user, final String tempPass)
	{
		return constructEmail("Account created", "Greetings " + user.getFirstName() + " " + user.getLastName() + "\r\nYour account has been created.\r\nYour temporary password is: " + tempPass + "\r\n", user.getEmail());
	}
}
