package io.ecx.workshop.spring.workshop.service;

import java.util.List;
import java.util.Optional;


import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.models.User;

public interface CommentService
{
    Comment findById(Integer id);
    List<Comment> findByBlogPost(BlogPost blogPost);
    List<Comment> findByUser(User user);
    List<Comment> findChildComments(Comment comment);
    Optional<Comment> createComment(User user, String content, BlogPost blogPost);
    Optional<Comment> createComment(User user, String content, Comment parentComment);
    Optional<Comment> createCommentForCurrentUser(String content, BlogPost blogPost);
    Optional<Comment> createCommentForCurrentUser(String content, Comment parentComment);
}
