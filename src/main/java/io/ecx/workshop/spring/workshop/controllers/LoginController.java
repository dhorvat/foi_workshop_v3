package io.ecx.workshop.spring.workshop.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.forms.NewUserForm;
import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.util.constants.ViewConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller that handles login mappings.
 */
@Controller
public class LoginController
{
    private final ModelAndView mav = new ModelAndView();

    /**
     * Show login page (login is done trough modal)
     * @return Model and view object.
     */
    @GetMapping(value = PathConstants.BasePaths.login)
    private ModelAndView login()
    {
        NewUserForm newUserForm = new NewUserForm();
        mav.setViewName(ViewConstants.Pages.home);
        mav.getModelMap().addAttribute("newUserForm", newUserForm);

        return mav;
    }

    /**
     * Log user out of the application.
     * @param request Http request
     * @param response Http response
     * @return Model and view object that redirects to home page.
     */
    @GetMapping(PathConstants.BasePaths.logout)
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response)
    {
        mav.setViewName(PathConstants.Redirect.home);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null)
        {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return mav;
    }
}
