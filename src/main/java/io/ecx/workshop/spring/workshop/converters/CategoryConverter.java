package io.ecx.workshop.spring.workshop.converters;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import io.ecx.workshop.spring.workshop.data.BlogPostData;
import io.ecx.workshop.spring.workshop.data.CategoryData;
import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.service.BlogPostService;

@Component
public class CategoryConverter implements Converter<Category, CategoryData>
{
    private final BlogPostService blogPostService;

    @Autowired
    public CategoryConverter(BlogPostService blogPostService)
    {
        this.blogPostService = blogPostService;
    }

    @Override
    public CategoryData convert(Category source)
    {
        CategoryData target = new CategoryData();

        target.setId(source.getId());
        target.setName(source.getName());
        target.setDescription(source.getDescription());

        List<BlogPost> blogPosts = blogPostService.findByCategory(source);
        List<BlogPostData> blogPostDatas = new ArrayList<>();

        if (!CollectionUtils.isEmpty(blogPosts))
        {
            for (BlogPost blogPost : filterActive(blogPosts))
            {
                BlogPostData blogPostData = new BlogPostData();
                blogPostData.setId(blogPost.getId());
                blogPostData.setTitle(blogPost.getTitle());
                blogPostDatas.add(blogPostData);
            }
            target.setBlogPostData(blogPostDatas);
        }

        return target;
    }

    /**
     * Filters out inactive BlogPosts.
     * @param blogPost List of blog's for filtering.
     * @return Filtered list of blog's.
     */
    private static List<BlogPost> filterActive(final List<BlogPost> blogPost)
    {
        //List<BlogPost> list = new ArrayList<>();
        //for (BlogPost post : blogPost)
        //{
        //	if (post.isActive())
        //	{
        //		list.add(post);
        //	}
        //}
        //return list;

        return blogPost.stream().filter(BlogPost::isActive).collect(Collectors.toList());  // ili filter(blogPost1 -> blogPost1.isActive())
    }
}
