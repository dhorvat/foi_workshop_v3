package io.ecx.workshop.spring.workshop.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.SecurityPaths.apiCategoryAll;
import static io.ecx.workshop.spring.workshop.util.constants.ApiPathConstants.SecurityPaths.apiUserAll;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.BasePaths.home;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.BasePaths.login;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.BasePaths.login_error;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.BasePaths.registration;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.adminAll;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.anyPath;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.basePath;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.categoryAll;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.cssAll;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.error;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.jsAll;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.postAll;
import static io.ecx.workshop.spring.workshop.util.constants.PathConstants.SecurityPaths.webjarsAll;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
    private final UserDetailsService userDetailsService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder)
    {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.authorizeRequests()
            .antMatchers(login, registration, login_error).not().authenticated()
            .antMatchers(home, basePath, postAll, categoryAll, apiCategoryAll, apiUserAll, jsAll , cssAll, webjarsAll, error).permitAll()
            .antMatchers(adminAll).hasRole("ADMIN")
            .antMatchers(  anyPath).authenticated()
            .and().formLogin().loginPage(login).failureForwardUrl(home)
            .and().exceptionHandling().accessDeniedPage(home)
            .and().logout().clearAuthentication(true).logoutSuccessUrl(home);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
}
