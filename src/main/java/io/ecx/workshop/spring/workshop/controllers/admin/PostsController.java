package io.ecx.workshop.spring.workshop.controllers.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.forms.NewBlogPostForm;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.security.service.SessionService;
import io.ecx.workshop.spring.workshop.service.BlogPostService;
import io.ecx.workshop.spring.workshop.service.CategoryService;
import io.ecx.workshop.spring.workshop.service.UserService;
import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.util.constants.ViewConstants;

import javax.validation.Valid;

/**
 * Controller that handles views for posts administration.
 */
@Controller
@RequestMapping(value = PathConstants.AdminPaths.adminRoot)
public class PostsController
{
    private final UserService userService;

    private final BlogPostService blogPostService;

    private final CategoryService categoryService;

    private final SessionService sessionService;

    private final ModelAndView mav = new ModelAndView();

    @Autowired
    public PostsController(UserService userService,
                           BlogPostService blogPostService,
                           CategoryService categoryService,
                           SessionService sessionService)
    {
        this.userService = userService;
        this.blogPostService = blogPostService;
        this.categoryService = categoryService;
        this.sessionService = sessionService;
    }

    /**
     * Show post administration page.
     * @return Model and view object to show post administration page.
     */
    @GetMapping(value = PathConstants.AdminPaths.posts)
    public ModelAndView posts()
    {
        mav.setViewName(ViewConstants.AdminPages.posts);
        mav.getModelMap().addAttribute("newBlogPostForm", new NewBlogPostForm());

        return mav;
    }

    /**
     * Update post information.
     * @param title Post title
     * @param content Post content
     * @param categoryId Id of category to assign blog post to.
     * @return Redirects to homepage.
     */
    @PostMapping(value = PathConstants.AdminPaths.savePost)
    public String savePost(@Valid NewBlogPostForm newBlogPostForm)
    {
        User user = userService.findByUsername(sessionService.getActiveUsername());
        Category category = categoryService.findById(newBlogPostForm.getCategoryId());

        blogPostService.createBlogPost(newBlogPostForm.getTitle(), newBlogPostForm.getContent(), user, category);

        return PathConstants.Redirect.posts;
    }
}
