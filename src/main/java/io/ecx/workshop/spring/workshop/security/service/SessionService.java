package io.ecx.workshop.spring.workshop.security.service;

public interface SessionService
{
    String getActiveUsername();
}
