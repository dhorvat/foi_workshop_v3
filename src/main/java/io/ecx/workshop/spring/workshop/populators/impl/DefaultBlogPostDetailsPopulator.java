package io.ecx.workshop.spring.workshop.populators.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.ecx.workshop.spring.workshop.data.BlogPostDetails;
import io.ecx.workshop.spring.workshop.data.CommentsData;
import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.populators.BlogPostDetailsPopulator;
import io.ecx.workshop.spring.workshop.service.CommentService;

/**
 * Populator that populates BlogPostDetails object with blog post and associated comments.
 */
@Component
public class DefaultBlogPostDetailsPopulator implements BlogPostDetailsPopulator
{
    @Autowired
    private CommentService commentService;

    /**
     * Populates list of BlogPostDetails.
     * @param source List of blog posts to populate.
     * @param target List of populated blog posts.
     */
    @Override
    public void populate(List<BlogPost> source, List<BlogPostDetails> target)
    {
        //for (BlogPost blogPost : source)
        //{
        //    BlogPostDetails blogPostDetails = new BlogPostDetails();
        //    populate(blogPost, blogPostDetails);
        //    target.add(blogPostDetails);
        //}

        source.forEach(blogPost -> {
            BlogPostDetails blogPostDetails = new BlogPostDetails();
            populate(blogPost, blogPostDetails);
            target.add(blogPostDetails);
        });
    }

    /**
     * Populates single BlogPostDetails.
     * @param source Source BlogPost instance.
     * @param target Target BlogPostDetails instance.
     */
    @Override
    public void populate(final BlogPost source, final BlogPostDetails target)
    {
        target.setBlogPost(source);
		//
        //List<CommentsData> list = new ArrayList<>();
        //for (Comment comment : commentService.findByBlogPost(source))
        //{
        //    CommentsData commentsData = getCommentsData(comment);
        //    list.add(commentsData);
        //}
        //target.setCommentsData(list);

        target.setCommentsData(commentService.findByBlogPost(source)
                                             .stream()
                                             .map(this::getCommentsData)  // ili .map(comment -> getCommentsData(comment))
                                             .collect(Collectors.toList()));
    }

    private CommentsData getCommentsData(final Comment comment)
    {
        CommentsData commentsData = new CommentsData();
        commentsData.setComment(comment);
        commentsData.setSubComments(commentService.findChildComments(comment));
        return commentsData;
    }
}
