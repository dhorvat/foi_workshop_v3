package io.ecx.workshop.spring.workshop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import io.ecx.workshop.spring.workshop.forms.NewUserForm;
import io.ecx.workshop.spring.workshop.util.constants.PathConstants;
import io.ecx.workshop.spring.workshop.util.constants.ViewConstants;

/**
 * Controller that handles profile mappings.
 */
@Controller
public class ProfileController
{
    private final ModelAndView mav = new ModelAndView();

    private final NewUserForm newUserForm = new NewUserForm();

    /**
     * Get details of currently logged in user.
     * @return Model and view object for profile details.
     */
    @GetMapping(value = PathConstants.Profile.selfProfile)
    public ModelAndView getSelfProfile()
    {
        /** TODO
         * - set view name (hint: ViewConstants)
         * - set newUserForm model attribute
         * - set profile model attribute to "/self"
         */

        return mav;
    }

    /**
     * Get details of selected user.
     * @param userId Id of the user to be shown.
     * @return Model and view object for selected user details.
     */
    @GetMapping(value = PathConstants.Profile.userProfile)
    public ModelAndView getUserProfile(@PathVariable("id") int userId)
    {
        /** TODO
         * - set view name (hint: ViewConstants)
         * - set newUserForm model attribute
         * - set profile model attribute to "/ + userId"
         */

        return mav;
    }
}
