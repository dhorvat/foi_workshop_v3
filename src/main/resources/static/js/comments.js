var header = $("meta[name='_csrf_header']").attr("content");
var token = $("meta[name='_csrf']").attr("content");
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

window.onload = function() {
	$('.submit-comment').on('click', function(event) {
		var id = event.target.id;
		var content = $(event.target).prev().val();

		if(id.match("^subComment_")) {
			sendSubComment(id.replace("subComment_", ''), content);
		}
		if(id.match("^blogComment_")) {
			sendRootComment(id.replace("blogComment_", ''), content);
		}
	});
}

function sendRootComment(id, comment) {
	$.ajax({
		type : "POST",
		url : baseUrl + "/api/comments/" + id + "/blog",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		dataType : "text",
		headers : {
			"X-CSRF-TOKEN" : token
		},
		data : {
			content : comment
		},
		success : function(response) {
			console.log("Comment added: " + response);
			location.reload()
		},
		error : function() {
			console.error("Error adding comment");
		}
	});
}

function sendSubComment(id, comment) {
	$.ajax({
		type : "POST",
		url : baseUrl + "/api/comments/" + id + "/comment",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		dataType : "text",
		headers : {
			"X-CSRF-TOKEN" : token
		},
		data : {
			content : comment
		},
		success : function(response) {
			console.log("Sub comment added: " + response);
			location.reload();
		},
		error : function() {
			console.error("Error sending sub comment");
		}
	});
}