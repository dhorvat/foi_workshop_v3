var header = $("meta[name='_csrf_header']").attr("content");
var token = $("meta[name='_csrf']").attr("content");
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

function fetchActivePosts() {
    $.ajax({
        type : "GET",
        url : baseUrl + "/api/post/posts/active",
        contentType : "application/json; charset=utf-8",
        dataType : "text",
        headers : {
            "X-CSRF-TOKEN" : token
        },
        success : function(response) {
            $('#table-active-posts > tbody').empty();
            console.log("Posts active fetched: " + response);
            $.each(JSON.parse(response), function(i, item) {
                var dateCreated = new Date(item.created);
                var dateUpdated = item.updated == null ? null : new Date(item.updated);
                $('#table-active-posts > tbody')
                    .append('<tr><td>'+item.id+
                    '</td><td>'+item.title+
                    '</td><td>'+item.user.username+
                    '</td><td>'+item.category.name+
                    '</td><td>'+dateCreated.format("dd.mmmm yyyy. HH:ss")+
                    '</td><td>'+(dateUpdated == null ? '-' : dateUpdated.format("dd.mmmm yyyy. HH:ss"))+
                    '</td><td><button type="button" class="btn btn-danger" onClick="toggle('+item.id+')">Deactivate</button></td></tr>');
            });
        },
        error : function() {
            console.error("Error fetching active posts");
        }
    });
}

function fetchInactivePosts() {
    $.ajax({
        type : "GET",
        url : baseUrl + "/api/post/posts/inactive",
        contentType : "application/json; charset=utf-8",
        dataType : "text",
        headers : {
            "X-CSRF-TOKEN" : token
        },
        success : function(response) {
            $('#table-inactive-posts > tbody').empty();
            console.log("Posts inactive fetched: " + response);
            $.each(JSON.parse(response), function(i, item) {
                var dateCreated = new Date(item.created);
                var dateUpdated = item.updated == null ? null : new Date(item.updated);
                $('#table-inactive-posts > tbody')
                    .append('<tr><td>'+item.id+
                        '</td><td>'+item.title+
                        '</td><td>'+item.user.username+
                        '</td><td>'+(item.category == null ? '' : item.category.name)+
                        '</td><td>'+dateCreated.format("dd.mmmm yyyy. HH:ss")+
                        '</td><td>'+(dateUpdated == null ? '-' : dateUpdated.format("dd.mmmm yyyy. HH:ss"))+
                        '</td><td><button type="button" class="btn btn-danger" onClick="toggle('+item.id+')">Activate</button></td></tr>');
            });
        },
        error : function() {
            console.error("Error fetching inactive posts");
        }
    });
}

function fetchCategoriesForSelect() {
	$.ajax({
		type : "GET",
		url : baseUrl + "/api/category/categories/active",
		contentType : "application/json; charset=utf-8",
		dataType : "text",
		headers : {
			"X-CSRF-TOKEN" : token
		},
		success : function(response) {
			$("#category-select").empty();
			$("#category-select").append('<option disabled="disabled" selected="selected">Choose...</option>');
			$.each(JSON.parse(response), function(i, item) {
				$("#category-select").append('<option value="'+item.id+'">'+item.name+'</option>')
			});
		},
		error : function() {
			console.error("Error fetching categories!");
		}
	});
}

function toggle(id) {
    $.ajax({
        type : "PATCH",
        url : baseUrl + "/api/post/"+id+"/toggle",
        contentType : "application/json; charset=utf-8",
        dataType : "text",
        headers : {
            "X-CSRF-TOKEN" : token
        },
        success : function(response) {
            console.log("Post toggled: " + response);
            location.reload();
        },
        error : function() {
            console.error("Error toggling post: " + id);
        }
    });
}

if (window.addEventListener)
{
  window.addEventListener('load', fetchActivePosts(), false);
  window.addEventListener('load', fetchInactivePosts(), false);
  window.addEventListener('load', fetchCategoriesForSelect(), false);
}
else if (window.attachEvent)
{
  window.attachEvent('onload', fetchActivePosts());
  window.attachEvent('onload', fetchInactivePosts());
  window.attachEvent('onload', fetchCategoriesForSelect());
}