function newNotif (nType, nMessage) {
    $.notify({
        message: nMessage
    }, {
        type: nType,
        delay: 1000,
        z_index: 2000
    });
}

function newLongNotif (nType, nMessage) {
    $.notify({
        message: nMessage
    }, {
        type: nType,
        delay: 7000,
        z_index: 2000
    });
}

function notifyInfo (nMessage) {
    newNotif("info", nMessage);
}

function notifyInfoLong (nMessage) {
    newLongNotif("info", nMessage);
}

function notifyFailure (nMessage) {
    newNotif("danger", nMessage);
}

function notifyFailureLong (nMessage) {
    newLongNotif("danger", nMessage);
}

function notifySuccess (nMessage) {
    newNotif("success", nMessage);
}

function notifySuccessLong (nMessage) {
    newLongNotif("success", nMessage);
}
