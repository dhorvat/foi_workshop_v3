var header = $("meta[name='_csrf_header']").attr("content");
var token = $("meta[name='_csrf']").attr("content");
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
if (window.addEventListener)
{
  window.addEventListener('load', getActiveCategories(), false);
}
else if (window.attachEvent)
{
  window.attachEvent('onload', getActiveCategories());
}

$(document).ready(function() {
	if(getUrl.href.indexOf("home") <= -1 && getUrl.href.indexOf("category") <= -1 ) {
     	$('#modal-login').modal('show');
	}
});

function getActiveCategories() {
	$.ajax({
		type : "GET",
		url : baseUrl + "/api/category/categories/active",
		contentType : "application/json; charset=utf-8",
		dataType : "text",
		headers : {
			"X-CSRF-TOKEN" : token
		},
		success : function(response) {
			$("#category-even").empty();
			$("#category-odd").empty();
			console.log("Categories fetched: " + response);
			$.each(JSON.parse(response), function(i, item) {
				if(i % 2 == 0) {
					$("#category-even").append('<li><a href="'+baseUrl+'/category/'+item.id+'">'+item.name+'</a></li>');
				} else {
					$("#category-odd").append('<li><a href="'+baseUrl+'/category/'+item.id+'">'+item.name+'</a></li>');
				}
			});
		},
		error : function() {
			console.error("Error fetching categories!");
		}
	});
}