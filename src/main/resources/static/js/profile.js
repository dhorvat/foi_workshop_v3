var header = $("meta[name='_csrf_header']").attr("content");
var token = $("meta[name='_csrf']").attr("content");
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

window.onload = function() {
	getProfileData();
}

function getProfileData() {
	var startUrl = baseUrl + "/api/user";

	$.ajax({
		type : "GET",
		url : startUrl + profile_url,
		contentType : "application/json; charset=utf-8",
		dataType : "text",
		headers : {
			"X-CSRF-TOKEN" : token
		},
		success : function(response) {
			console.log("Profile data fetched: " + response);
			var json = JSON.parse(response);
			$('#textbox-username').val(json.username);
			$('#textbox-email').val(json.email);
			$('#textbox-firstname').val(json.firstName);
			$('#textbox-lastname').val(json.lastName);
		},
		error : function () {
			console.error("Error fetching user " + profile_url);
		}
	});
}