var header = $("meta[name='_csrf_header']").attr("content");
var token = $("meta[name='_csrf']").attr("content");
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

function fetchActiveUsers() {
    $.ajax({
        type : "GET",
        url : baseUrl + "/api/user/users/active",
        contentType : "application/json; charset=utf-8",
        dataType : "text",
        headers : {
            "X-CSRF-TOKEN" : token
        },
        success : function(response) {
            $('#table-active-users > tbody').empty();
            console.log("Users fetched: " + response);
            $.each(JSON.parse(response), function(i, item){
                $('#table-active-users > tbody')
                    .append('<tr><td>'+item.id+'</td><td>'
                        +item.firstName+'</td><td>'
                        +item.lastName+'</td><td>'
                        +item.username+'</td><td>'
                        +item.email+'</td><td><button type="button" class="btn btn-danger" onClick="toggle('+item.id+')">Deactivate</button></td></tr>');
            });
        },
        error : function() {
            console.error("Error fetching users");
        }
    });
}

function fetchInactiveUsers() {
    $.ajax({
        type : "GET",
        url : baseUrl + "/api/user/users/inactive",
        contentType : "application/json; charset=utf-8",
        dataType : "text",
        headers : {
            "X-CSRF-TOKEN" : token
        },
        success : function(response) {
            $('#table-inactive-users > tbody').empty();
            console.log("Users fetched: " + response);
            $.each(JSON.parse(response), function(i, item){
                $('#table-inactive-users > tbody')
                    .append('<tr><td>'+item.id+'</td><td>'
                        +item.firstName+'</td><td>'
                        +item.lastName+'</td><td>'
                        +item.username+'</td><td>'
                        +item.email+'</td><td><button type="button" class="btn btn-info" onClick="toggle('+item.id+')">Activate</button></td></tr>');
            });
        },
        error : function() {
            console.error("Error fetching users");
        }
    });
}

function toggle(id) {
    $.ajax({
        type : "PATCH",
        url : baseUrl + "/api/user/"+id+"/toggle",
        contentType : "application/json; charset=utf-8",
        dataType : "text",
        headers : {
            "X-CSRF-TOKEN" : token
        },
        success : function(response) {
            console.log("User toggled: " + response);
            location.reload();
        },
        error : function() {
            console.error("Error toggling user: " + id);
        }
    });
}

if (window.addEventListener)
{
  window.addEventListener('load', fetchActiveUsers(), false);
  window.addEventListener('load', fetchInactiveUsers(), false);
}
else if (window.attachEvent)
{
  window.attachEvent('onload', fetchActiveUsers());
  window.attachEvent('onload', fetchInactiveUsers());
}