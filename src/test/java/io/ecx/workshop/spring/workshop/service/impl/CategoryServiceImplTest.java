package io.ecx.workshop.spring.workshop.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import io.ecx.workshop.spring.workshop.exceptions.CategoryExistsException;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.repository.CategoryRepostory;
import io.ecx.workshop.spring.workshop.service.ModelService;

import static io.ecx.workshop.spring.workshop.utils.CreationUtils.CategoryMock.TEMP_CATEGORY_DESCRIPTION;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.CategoryMock.TEMP_CATEGORY_ID;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.CategoryMock.TEMP_CATEGORY_NAME;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.CategoryMock.createDefaultCategory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author d.horvat
 * @since 16-08-2019
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.Silent.class)
public class CategoryServiceImplTest
{
    @Mock
    private CategoryRepostory categoryRepostory;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private CategoryServiceImpl categoryService;

    private Category category;

    @Before
    public void setUp()
    {
        category = createDefaultCategory();
    }

    @Test
    public void findByIdTest()
    {
        when(categoryRepostory.findById(TEMP_CATEGORY_ID)).thenReturn(Optional.of(category));

        assertThat(categoryService.findById(TEMP_CATEGORY_ID)).isEqualTo(category);
    }

    @Test
    public void findByNameTest()
    {
        when(categoryRepostory.findByName(TEMP_CATEGORY_NAME)).thenReturn(category);

        assertThat(categoryService.findByName(TEMP_CATEGORY_NAME)).isEqualTo(category);
    }

    @Test
    public void findAllActiveTest()
    {
        List<Category> categories = Collections.singletonList(category);
        when(categoryRepostory.findByActive(true)).thenReturn(categories);

        assertThat(categoryService.findAllActive()).isEqualTo(categories);
    }

    @Test
    public void findAllInactiveTest()
    {
        List<Category> categories = Collections.singletonList(category);
        when(categoryRepostory.findByActive(false)).thenReturn(categories);

        assertThat(categoryService.findAllInactive()).isEqualTo(categories);
    }

    @Test
    public void createCategoryTest()
    {
        // CASE 1: Category with name exists
        when(categoryRepostory.findByName(TEMP_CATEGORY_NAME)).thenReturn(category);

        assertThatThrownBy(() -> categoryService.createCategory(TEMP_CATEGORY_NAME, TEMP_CATEGORY_DESCRIPTION)).isInstanceOf(CategoryExistsException.class);

        // CASE 2: Category with name doesn't exist
        when(categoryRepostory.findByName(TEMP_CATEGORY_NAME)).thenReturn(null);
        when(modelService.save(any())).thenReturn(category);

        Category result = categoryService.createCategory(TEMP_CATEGORY_NAME, TEMP_CATEGORY_DESCRIPTION);
        assertThat(result.getName()).isEqualTo(TEMP_CATEGORY_NAME);
        assertThat(result.getDescription()).isEqualTo(TEMP_CATEGORY_DESCRIPTION);
    }
}
