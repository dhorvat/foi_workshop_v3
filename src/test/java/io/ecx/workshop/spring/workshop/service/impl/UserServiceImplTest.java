package io.ecx.workshop.spring.workshop.service.impl;

import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import io.ecx.workshop.spring.workshop.exceptions.UserNotFoundException;
import io.ecx.workshop.spring.workshop.models.Role;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.repository.RoleRepository;
import io.ecx.workshop.spring.workshop.repository.UserRepository;
import io.ecx.workshop.spring.workshop.service.ModelService;
import io.ecx.workshop.spring.workshop.utils.CreationUtils.UserMock;

import static io.ecx.workshop.spring.workshop.util.constants.enums.RoleType.ROLE_USER;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.UserMock.TEMP_ADMIN_EMAIL;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.UserMock.TEMP_ADMIN_FIRSTNAME;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.UserMock.TEMP_ADMIN_ID;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.UserMock.TEMP_ADMIN_LASTNAME;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.UserMock.TEMP_ADMIN_PASS;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.UserMock.TEMP_ADMIN_USERNAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author d.horvat
 * @since 16-08-2019
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceImplTest
{
    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private UserServiceImpl userService;

    private User user;

    @Before
    public void setUp()
    {
        user = UserMock.createAdminUser();
    }

    @Test
    public void findByUsernameTest()
    {
        when(userRepository.findByUsername(TEMP_ADMIN_USERNAME)).thenReturn(user);

        assertThat(userService.findByUsername(TEMP_ADMIN_USERNAME)).isEqualTo(user);
    }

    @Test
    public void findByEmailTest()
    {
        when(userRepository.findByEmail(TEMP_ADMIN_EMAIL)).thenReturn(user);

        assertThat(userService.findByEmail(TEMP_ADMIN_EMAIL)).isEqualTo(user);
    }

    @Test
    public void findByIdTest()
    {
        when(userRepository.findById(TEMP_ADMIN_ID)).thenReturn(Optional.of(user));

        assertThat(userService.findById(TEMP_ADMIN_ID)).isEqualTo(Optional.of(user));
    }

    @Test
    public void createUserTest()
    {
        final String encodedPass = "cryptedPass";

        when(modelService.save(any())).thenReturn(user);
        when(roleRepository.findByName(ROLE_USER.name())).thenReturn(mock(Role.class));
        when(bCryptPasswordEncoder.encode(TEMP_ADMIN_PASS)).thenReturn(encodedPass);
        User result = userService.createUser(TEMP_ADMIN_USERNAME, TEMP_ADMIN_FIRSTNAME, TEMP_ADMIN_LASTNAME, TEMP_ADMIN_EMAIL, TEMP_ADMIN_PASS, true);

        assertThat(result).isEqualTo(user);
    }

    @Test
    public void findByActiveTest()
    {
        when(userRepository.findByActive(true)).thenReturn(Collections.singletonList(user));

        assertThat(userService.findByActive(true)).contains(user);
    }

    @Test
    public void updateUserTest()
    {
        // CASE 1: user not found
        when(userRepository.findById(TEMP_ADMIN_ID)).thenReturn(null);

        assertThatThrownBy(() -> userService.updateUser(user)).isInstanceOf(UserNotFoundException.class);
        verify(modelService, never()).save(any());

        // CASE 2: user found
        when(userRepository.findById(TEMP_ADMIN_ID)).thenReturn(Optional.of(user));
        when(modelService.save(user)).thenReturn(user);

        assertThat(userService.updateUser(user)).isEqualTo(user);
        verify(modelService, atLeastOnce()).save(user);
    }

    @Test
    public void toggleActiveTest()
    {
        // CASE 1: User not found
        when(userRepository.findById(TEMP_ADMIN_ID)).thenReturn(null);

        assertThat(userService.toggleActive(TEMP_ADMIN_ID)).isNotPresent();
        // CASE 2: User found
        when(userRepository.findById(TEMP_ADMIN_ID)).thenReturn(Optional.of(user));

        assertThat(userService.toggleActive(TEMP_ADMIN_ID)).isEqualTo(Optional.of(user));
    }
}
