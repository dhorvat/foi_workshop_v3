package io.ecx.workshop.spring.workshop.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import org.mockito.Mock;

import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.models.User;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreationUtils
{
    public static class UserMock
    {
        public static final Integer TEMP_ADMIN_ID = 1;
        public static final String TEMP_ADMIN_EMAIL = "admin@admin.hr";
        public static final String TEMP_ADMIN_FIRSTNAME = "Admin";
        public static final String TEMP_ADMIN_LASTNAME = "Adminiac";
        public static final String TEMP_ADMIN_PASS = "nimda";
        public static final String TEMP_ADMIN_USERNAME = "admin";

        public static final Integer TEMP_USER_ID = 2;
        public static final String TEMP_USER_EMAIL = "user@user.hr";
        public static final String TEMP_USER_FIRSTNAME = "User";
        public static final String TEMP_USER_LASTNAME = "Userman";
        public static final String TEMP_USER_PASS = "resu";
        public static final String TEMP_USER_USERNAME = "user";

        private static User createUser(final Integer id, final String password, final String email, final String lastName, final String firstName, final String username)
        {
            User user = mock(User.class);

            when(user.getId()).thenReturn(id);
            when(user.getPassword()).thenReturn(password);
            when(user.getEmail()).thenReturn(email);
            when(user.getLastName()).thenReturn(lastName);
            when(user.getFirstName()).thenReturn(firstName);
            when(user.getUsername()).thenReturn(username);

            return user;
        }

        public static User createAdminUser()
        {
            return createUser(TEMP_ADMIN_ID, TEMP_ADMIN_PASS, TEMP_ADMIN_EMAIL, TEMP_ADMIN_LASTNAME, TEMP_ADMIN_FIRSTNAME, TEMP_ADMIN_USERNAME);
        }

        public static User createNewUser()
        {
            return createUser(TEMP_USER_ID, TEMP_USER_PASS, TEMP_USER_EMAIL, TEMP_USER_LASTNAME, TEMP_USER_FIRSTNAME, TEMP_USER_USERNAME);
        }
    }

    public static class BlogPostMock
    {
        public static final Integer TEMP_BLOG_ID = 1;
        public static final String TEMP_BLOG_TITLE = "TEST TITLE";
        public static final String TEMP_BLOG_CONTENT = "TEST TEXT: Some medium text used to test testing tests with testing text for tests";

        public static final Integer TEMP_ANTOHER_BLOG_ID = 2;
        public static final String TEMP_ANOTHER_BLOG_TITLE = "TEST TITLE";
        public static final String TEMP_ANTOHER_BLOG_CONTENT = "TEST TEXT: Some medium text used to test testing tests with testing text for tests";

        private static BlogPost createBlogPost(final Integer id, final String title, final String content, final User user)
        {
            BlogPost blogPost = mock(BlogPost.class);

            when(blogPost.getId()).thenReturn(id);
            when(blogPost.getTitle()).thenReturn(title);
            when(blogPost.getContent()).thenReturn(content);
            when(blogPost.getUser()).thenReturn(user);

            return blogPost;
        }

        public static BlogPost createOneBlogForAdmin()
        {
            return createBlogPost(TEMP_BLOG_ID, TEMP_BLOG_TITLE, TEMP_BLOG_CONTENT, UserMock.createAdminUser());
        }

        public static  BlogPost createAnotherBlogForAdmin()
        {
            return createBlogPost(TEMP_ANTOHER_BLOG_ID, TEMP_ANOTHER_BLOG_TITLE, TEMP_ANTOHER_BLOG_CONTENT, UserMock.createAdminUser());
        }
    }

    public static class CategoryMock
    {
        public static final Integer TEMP_CATEGORY_ID = 1;
        public static final String TEMP_CATEGORY_DESCRIPTION = "TEST DESCRIPTION";
        public static final String TEMP_CATEGORY_NAME = "CATEGORY";

        public static Category createCategory(final Integer id, final String description, final String name)
        {
            Category category = mock(Category.class);

            when(category.getId()).thenReturn(id);
            when(category.getDescription()).thenReturn(description);
            when(category.getName()).thenReturn(name);

            return category;
        }

        public static Category createDefaultCategory()
        {
            return createCategory(TEMP_CATEGORY_ID, TEMP_CATEGORY_DESCRIPTION, TEMP_CATEGORY_NAME);
        }
    }

    public static class CommentMock
    {
        public static final Integer TEMP_FIRST_COMMENT_ID = 1;
        public static final String TEMP_FIRST_COMMENT_CONTENT = "TEXT FIRST COMMENTS TEXT with some text to test it out";
        public static final Integer TEMP_SECOND_COMMENT_ID = 2;
        public static final String TEMP_SECOND_COMMENT_CONTENT = "TEXT SECOND COMMENTS TEXT with some text to test it out";
        public static final Integer TEMP_THIRD_COMMENT_ID = 3;
        public static final String TEMP_THIRD_COMMENT_CONTENT = "TEXT THIRD COMMENTS TEXT with some text to test it out";

        public static final boolean TEMP_COMMENT_ACTIVE = true;
        public static final boolean TEMP_COMMENT_INACTIVE = false;

        private static Comment createComment(final Integer id, final String content, final BlogPost blogPost, final User user, final boolean active)
        {
            Comment comment = mock(Comment.class);

            when(comment.getId()).thenReturn(id);
            when(comment.getContent()).thenReturn(content);
            when(comment.getBlogPost()).thenReturn(blogPost);
            when(comment.getUser()).thenReturn(user);
            when(comment.isActive()).thenReturn(active);

            return  comment;
        }

        private static Comment createSubComment(final Integer id, final String content, final Comment parent, final User user, final boolean active)
        {
            Comment comment = mock(Comment.class);

            when(comment.getId()).thenReturn(id);
            when(comment.getContent()).thenReturn(content);
            when(comment.getComment()).thenReturn(parent);
            when(comment.getUser()).thenReturn(user);
            when(comment.isActive()).thenReturn(active);

            return comment;
        }

        public static Comment createDefaultComment()
        {
            User user = UserMock.createAdminUser();
            BlogPost blogPost = BlogPostMock.createOneBlogForAdmin();

            return createComment(TEMP_FIRST_COMMENT_ID, TEMP_FIRST_COMMENT_CONTENT, blogPost, user, true);
        }

        public static Comment createDefaultChildComment(final Comment parent)
        {
            User user = UserMock.createAdminUser();

            return createSubComment(TEMP_SECOND_COMMENT_ID, TEMP_SECOND_COMMENT_CONTENT, parent, user, true);
        }

        public static List<Comment> createThreeActiveAdminComments()
        {
            User admin = UserMock.createAdminUser();
            BlogPost blogPost = BlogPostMock.createOneBlogForAdmin();

            return Arrays.asList(CommentMock.createComment(TEMP_FIRST_COMMENT_ID, TEMP_FIRST_COMMENT_CONTENT, blogPost, admin, TEMP_COMMENT_ACTIVE),
                    CommentMock.createComment(TEMP_SECOND_COMMENT_ID, TEMP_SECOND_COMMENT_CONTENT, blogPost, admin, TEMP_COMMENT_ACTIVE),
                    CommentMock.createComment(TEMP_THIRD_COMMENT_ID, TEMP_THIRD_COMMENT_CONTENT, blogPost, admin, TEMP_COMMENT_ACTIVE));
        }

        public static List<Comment> createTwoActiveOneInactiveAdminComments()
        {
            User admin = UserMock.createAdminUser();
            BlogPost blogPost = BlogPostMock.createOneBlogForAdmin();

            return Arrays.asList(CommentMock.createComment(TEMP_FIRST_COMMENT_ID, TEMP_FIRST_COMMENT_CONTENT, blogPost, admin, TEMP_COMMENT_ACTIVE),
                    CommentMock.createComment(TEMP_SECOND_COMMENT_ID, TEMP_SECOND_COMMENT_CONTENT, blogPost, admin, TEMP_COMMENT_ACTIVE),
                    CommentMock.createComment(TEMP_THIRD_COMMENT_ID, TEMP_THIRD_COMMENT_CONTENT, blogPost, admin, TEMP_COMMENT_INACTIVE));
        }
    }
}
