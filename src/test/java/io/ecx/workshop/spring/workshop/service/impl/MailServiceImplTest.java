package io.ecx.workshop.spring.workshop.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;

import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.utils.CreationUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author d.horvat
 * @since 16-08-2019
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.Silent.class)
public class MailServiceImplTest
{
    @Mock
    private Environment env;

    @InjectMocks
    private MailServiceImpl mailService;

    private static final String MAIL_USERNAME_KEY = "spring.mail.username";

    private static final String MAIL_USERNAME_VALUE = "test@test.com";

    private User user;

    @Before
    public void setUp()
    {
        when(env.getProperty(MAIL_USERNAME_KEY)).thenReturn(MAIL_USERNAME_VALUE);
        user = CreationUtils.UserMock.createAdminUser();
    }

    @Test
    public void constructEmailTest()
    {
        final String testSubject = "Subject";
        final String testBody = "Body of the mail";
        final String testTo = "to@test.com";

        SimpleMailMessage result = mailService.constructEmail(testSubject, testBody, testTo);

        assertThat(result.getSubject()).isEqualTo(testSubject);
        assertThat(result.getText()).isEqualTo(testBody);
        assertThat(result.getTo()).contains(testTo);
        assertThat(result.getFrom()).isEqualTo(MAIL_USERNAME_VALUE);
    }

    @Test
    public void constructTempPassEmailTest()
    {
        final String testPass = "asdkljasldkja";

        SimpleMailMessage result = mailService.constructTempPassEmail(user, testPass);

        assertThat(result.getText()).contains(user.getFirstName());
        assertThat(result.getText()).contains(user.getLastName());
        assertThat(result.getText()).contains(testPass);
    }
}
