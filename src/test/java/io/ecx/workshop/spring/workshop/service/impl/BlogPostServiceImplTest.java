package io.ecx.workshop.spring.workshop.service.impl;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Category;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.repository.BlogPostRepository;
import io.ecx.workshop.spring.workshop.service.ModelService;

import static io.ecx.workshop.spring.workshop.utils.CreationUtils.BlogPostMock.TEMP_BLOG_ID;
import static io.ecx.workshop.spring.workshop.utils.CreationUtils.BlogPostMock.createOneBlogForAdmin;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author d.horvat
 * @since 14-08-2019
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.Silent.class)
public class BlogPostServiceImplTest
{
    @Mock
    private BlogPostRepository blogPostRepository;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private BlogPostServiceImpl blogPostService;

    private BlogPost blogPost;

    private Category category;

    @Before
    public void setUp()
    {
        blogPost = createOneBlogForAdmin();
        category = mock(Category.class);
    }

    @Test
    public void findByIdTest()
    {
        final Integer id = blogPost.getId();

        when(blogPostRepository.findById(id)).thenReturn(Optional.of(blogPost));

        assertThat(blogPostService.findById(id)).isEqualTo(Optional.of(blogPost));
    }

    @Test
    public void findByTitleTest()
    {
        final String title = blogPost.getTitle();

        when(blogPostRepository.findByTitle(title)).thenReturn(blogPost);

        assertThat(blogPostService.findByTitle(title)).isEqualTo(blogPost);
    }

    @Test
    public void findByUserTest()
    {
        final User user = blogPost.getUser();

        when(blogPostRepository.findByUser(user)).thenReturn(Collections.singletonList(blogPost));

        assertThat(blogPostService.findByUser(user)).contains(blogPost);
    }

    @Test
    public void createBlogPostTest()
    {
        when(modelService.save(any())).thenReturn(blogPost);

        assertThat(blogPostService.createBlogPost(
                blogPost.getTitle(),
                blogPost.getContent(),
                blogPost.getUser(),
                category))

                .isEqualTo(blogPost);
    }

    @Test
    public void findAll()
    {
        when(blogPostRepository.findAll()).thenReturn(Collections.singletonList(blogPost));

        assertThat(blogPostService.findAll()).contains(blogPost);
    }

    @Test
    public void findAllActiveTest()
    {
        when(blogPostRepository.findByActive(true)).thenReturn(Collections.singletonList(blogPost));

        assertThat(blogPostService.findAllActive()).contains(blogPost);
    }

    @Test
    public void findAllInactiveTest()
    {
        when(blogPostRepository.findByActive(false)).thenReturn(Collections.singletonList(blogPost));

        assertThat(blogPostService.findAllInactive()).contains(blogPost);
    }

    @Test
    public void findByCategoryTest()
    {
        when(blogPostRepository.findByCategory(category)).thenReturn(Collections.singletonList(blogPost));

        assertThat(blogPostService.findByCategory(category)).contains(blogPost);
    }

    @Test
    public void toggleActiveTest()
    {
        // CASE 1: Blog post not found
        when(blogPostRepository.findById(TEMP_BLOG_ID)).thenReturn(null);

        assertThat(blogPostService.toggleActive(TEMP_BLOG_ID)).isNotPresent();
        // CASE 2: Blog post found
        when(blogPostRepository.findById(TEMP_BLOG_ID)).thenReturn(Optional.of(blogPost));

        assertThat(blogPostService.toggleActive(TEMP_BLOG_ID)).isEqualTo(Optional.of(blogPost));
    }
}
