package io.ecx.workshop.spring.workshop.service.impl;

import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import io.ecx.workshop.spring.workshop.models.BlogPost;
import io.ecx.workshop.spring.workshop.models.Comment;
import io.ecx.workshop.spring.workshop.models.User;
import io.ecx.workshop.spring.workshop.repository.CommentRepository;
import io.ecx.workshop.spring.workshop.security.service.SessionService;
import io.ecx.workshop.spring.workshop.service.ModelService;
import io.ecx.workshop.spring.workshop.service.UserService;
import io.ecx.workshop.spring.workshop.utils.CreationUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author d.horvat
 * @since 16-08-2019
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.Silent.class)
public class CommentServiceImplTest
{
    @Mock
    private CommentRepository commentRepository;

    @Mock
    private ModelService modelService;

    @Mock
    private SessionService sessionService;

    @Mock
    private UserService userService;

    @InjectMocks
    private CommentServiceImpl commentService;

    private Comment comment;

    private Comment subComment;

    private BlogPost blogPost;

    private User user;

    @Before
    public void setUp()
    {
        comment = CreationUtils.CommentMock.createDefaultComment();
        subComment = CreationUtils.CommentMock.createDefaultChildComment(comment);
        blogPost = CreationUtils.BlogPostMock.createOneBlogForAdmin();
        user = CreationUtils.UserMock.createAdminUser();
    }

    @Test
    public void findByIdTest()
    {
        when(commentRepository.findById(CreationUtils.CommentMock.TEMP_FIRST_COMMENT_ID)).thenReturn(Optional.of(comment));

        assertThat(commentService.findById(CreationUtils.CommentMock.TEMP_FIRST_COMMENT_ID)).isEqualTo(comment);
    }

    @Test
    public void findByBlogPost()
    {
        when(commentRepository.findByBlogPost(blogPost)).thenReturn(Collections.singletonList(comment));

        assertThat(commentService.findByBlogPost(blogPost)).contains(comment);
    }

    @Test
    public void findByUserTest()
    {
        when(commentRepository.findByUser(user)).thenReturn(Collections.singletonList(comment));

        assertThat(commentService.findByUser(user)).contains(comment);
    }

    @Test
    public void findChildCommentsTest()
    {
        when(commentRepository.findByComment(comment)).thenReturn(Collections.singletonList(subComment));

        assertThat(commentService.findChildComments(comment)).contains(subComment);
    }

    @Test
    public void createCommentTest()
    {
        when(modelService.save(any())).thenReturn(comment);

        final Comment result = commentService.createComment(user, CreationUtils.CommentMock.TEMP_FIRST_COMMENT_CONTENT, blogPost).get();

        assertThat(result.getBlogPost().getId()).isEqualTo(blogPost.getId());
        assertThat(result).isEqualTo(comment);
    }

    @Test
    public void createSubCommentTest()
    {
        when(modelService.save(any())).thenReturn(subComment);

        final Comment result = commentService.createComment(user, CreationUtils.CommentMock.TEMP_SECOND_COMMENT_CONTENT, comment).get();

        assertThat(result.getComment().getId()).isEqualTo(comment.getId());
        assertThat(result).isEqualTo(subComment);
    }

    @Test
    public void createCommentForCurrentUserTest()
    {
        when(sessionService.getActiveUsername()).thenReturn(CreationUtils.UserMock.TEMP_ADMIN_USERNAME);
        when(userService.findByUsername(CreationUtils.UserMock.TEMP_ADMIN_USERNAME)).thenReturn(user);
        when(modelService.save(any())).thenReturn(comment);

        assertThat(commentService.createCommentForCurrentUser(CreationUtils.CommentMock.TEMP_FIRST_COMMENT_CONTENT, blogPost)).isEqualTo(Optional.of(comment));
    }

    @Test
    public void createSubCommentForCurrentUserTest()
    {
        when(sessionService.getActiveUsername()).thenReturn(CreationUtils.UserMock.TEMP_ADMIN_USERNAME);
        when(userService.findByUsername(CreationUtils.UserMock.TEMP_ADMIN_USERNAME)).thenReturn(user);
        when(modelService.save(any())).thenReturn(subComment);

        assertThat(commentService.createCommentForCurrentUser(CreationUtils.CommentMock.TEMP_FIRST_COMMENT_CONTENT, comment)).isEqualTo(Optional.of(subComment));
    }
}
